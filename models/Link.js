const mongoose = require('mongoose');

const schema = mongoose.Schema({
  name: { type: String, required: true },
  group: { type: mongoose.Schema.Types.ObjectId, ref: 'Group', required: true },
  identifier: { type: String, required: true }
}, { timestamps: {} });

exports.model = { name: 'Link', schema, subdocument: true };