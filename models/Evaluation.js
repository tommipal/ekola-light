const mongoose = require('mongoose');

const schema = mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  groups: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Group' }],
  archived: { type: Boolean, required: true, default: false },
}, { timestamps: {} });

exports.model = { name: 'Evaluation', schema };