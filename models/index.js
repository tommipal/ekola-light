const mongoose = require('mongoose');
const { config, Utils } = require('common');

exports.load = async function () {
  const uri = `mongodb://${config.databaseUser}:${config.databasePassword}@${config.databaseHost}:27017/${config.database}`;

  await mongoose.connect(uri, config.databaseConfig).then(() => {
    console.info(`\nMongoose connected successfully!`);

    console.info(`Models index: Loading models..`);
    const models = Utils.requireNamespace('models', 'model');

    for (let [name, model] of Utils.iterateObject(models)) {
      if (!model.subdocument) console.info(`Loading model: ${name}`);
      else console.info(`Loading model: ${name} (submodel)`);
      model = mongoose.model(name, model.schema);

      if (!global[name]) {
        Object.defineProperty(global, name, {
          enumerable: true,
          writable: false,
          value: model
        });
      }
    }
  },
    (err) => {
      console.error(console, `Mongoose connection error: ${err}`);
    });
}