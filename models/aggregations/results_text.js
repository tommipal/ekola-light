exports.pipeline = function (survey) {
  if (!survey) throw new Error();
  return [
    {
      $match: { _id: survey._id || survey },
    }, {
      $unwind: '$sessions'
    }, {
      $unwind: '$sessions.answers'
    }, {
      $unwind: '$sessions.answers.subanswers'
    }, {
      $match: { 'sessions.answers.subanswers.type': 'text' }
    }, {
      $group: {
        _id: {
          question: '$sessions.answers.question',
          question_name: '$sessions.answers.name',
          criteria: '$sessions.answers.subanswers.criteria',
          criteria_name: '$sessions.answers.subanswers.name',
          type: '$sessions.answers.subanswers.type',
          group: '$sessions.group',
          group_name: '$sessions.group_name',
        },
        answers: { $addToSet: '$sessions.answers.subanswers._id' },
        values: { $addToSet: '$sessions.answers.subanswers.value' },
        count: { $sum: 1 }
      }
    }
  ];
}