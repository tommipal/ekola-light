exports.pipeline = function (survey) {
  if (!survey) throw new Error();
  return [
    {
      $match: { _id: survey._id || survey },
    }, {
      $unwind: '$sessions'
    }, {
      $unwind: '$sessions.answers'
    }, {
      $unwind: '$sessions.answers.subanswers'
    }, {
      $unwind: '$sessions.answers.subanswers.value'
    }, {
      $match: { 'sessions.answers.subanswers.type': 'checkbox' }
    }, {
      $group: {
        _id: {
          question: '$sessions.answers.question',
          question_name: '$sessions.answers.name',
          criteria: '$sessions.answers.subanswers.criteria',
          criteria_name: '$sessions.answers.subanswers.name',
          type: '$sessions.answers.subanswers.type',
          group: '$sessions.group',
          group_name: '$sessions.group_name',
          value: '$sessions.answers.subanswers.value',
        },
        answers: { $addToSet: '$sessions.answers.subanswers._id' },
        count: { $sum: 1 }
      }
    }
  ];
}