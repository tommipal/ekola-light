const moment = require('moment');

const mongoose = require('mongoose');
const { Utils } = require('common');

const schema = mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  iv: { type: String, required: true },
  verified: { type: Boolean, required: true, default: false },
  admin: { type: Boolean, required: true, default: false },
}, { timestamps: {} });

/**
 * @param {String} pwd
 * @returns {Promise<{ encrypted:string, iv:string}>}
 */
schema.statics.hashpwd = async function (pwd) {
  if (!pwd || pwd.length < 1) throw new Error();

  return await Utils.encrypt(pwd);
};

/**
 * @returns {moment}
 */
schema.methods.getCreatedAt = function () {
  return moment(this.createdAt);
}

/**
 * @param {String} email
 * @param {String} pwd
 * @returns {Promise<Boolean>} match
 */
schema.statics.match = async function (email, pwd) {
  if (!email || !pwd) return false;
  const user = await User.findOne({ email: email });
  if (!user) return false;

  const decrypted = await Utils.decrypt(user.password, user.iv);
  return pwd === decrypted;
}

/**
 * @param {String} email
 * @param {String} id
 * @returns {Promise<Boolean>}
 */
schema.statics.isVerified = async function ({ id, email } = {}) {
  if (!email && !id) return false;

  let user;
  if (id) user = await User.findOne({ _id: id });
  else user = await User.findOne({ email: email });

  return (!user || !user.verified) ? false : true;
}

exports.model = { name: 'User', schema };