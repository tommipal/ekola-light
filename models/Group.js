const mongoose = require('mongoose');

/**
 * Subdocument
 */
const memberSchema = mongoose.Schema({
  email: { type: String, required: true },
  name: { type: String },
  description: { type: String },
}, { timestamps: {} });

const groupSchema = mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  archived: { type: Boolean, required: true, default: false },
  members: [memberSchema]
}, { timestamps: {} });

exports.model = { name: 'Group', schema: groupSchema };