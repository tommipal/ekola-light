const mongoose = require('mongoose');
const criteriaSchema = require('./Criteria').model.schema;

/**
 * Subdocument: question
 */
const schema = mongoose.Schema({
  name: { type: String, required: true, default: 'New Question' },
  description: { type: String },
  position: { type: Number, required: true },
  criteria: [criteriaSchema],
}, { timestamps: {} });

schema.methods.addCriteria = function (type) {
  let criteria = { name: 'New Criteria' };
  switch (type) {
    case 'select':
      criteria.type = type;
      break;

    case 'checkbox':
      criteria.type = type;
      break;

    case 'text':
      criteria.type = type;
      break;

    case 'scale':
      criteria.type = 'scale';
      criteria.minValue = 0;
      criteria.maxValue = 5;
      break;

    default:
      throw new Error(`Found an unrecognized criteria type: ${type}`);
  }

  this.criteria.push(criteria);
}

exports.model = { name: 'Question', schema, subdocument: true };