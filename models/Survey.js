const exceljs = require('exceljs');
const moment = require('moment');
const mongoose = require('mongoose');
const { Utils, Emailer } = require('common');

const linkSchema = require('./Link').model.schema;
const questionSchema = require('./Question').model.schema;
const sessionSchema = require('./Session').model.schema;

/**
 * Schema: Survey
 */
const schema = mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  evaluation: { type: mongoose.Schema.Types.ObjectId, ref: 'Evaluation', required: false },
  openingDate: { type: Date, required: true, default: moment().subtract(1, 'days').toDate() },
  closingDate: { type: Date, required: true, default: moment().subtract(1, 'days').toDate() },
  archived: { type: Boolean, required: true, default: false },
  public: { type: Boolean, required: true, default: false },
  questions: [questionSchema],
  links: [linkSchema],
  sessions: [sessionSchema],
}, { timestamps: {} });

schema.methods.addQuestion = function (data = {}) {
  data.position = this.questions.length;
  this.questions.push(data);

  return Utils.last(this.questions);
}

/**
 * @param {String|ObjectId|Survey} survey
 * @param {String|ObjectId|User} user
 * @param {Boolean} save
 * @returns {Promise<Survey>} survey
 */
schema.statics.copy = async function (survey, user, save = false) {
  if (!survey || !user) throw new Error(`Survey.copy: Missing parameters! Expected both survey and user. Found ${survey} and ${user}`);
  survey = await Survey.findOne({ _id: survey._id || survey });
  if (!survey) throw new Error(`Survey.copy: Survey doesn't seem to exist anymore`);

  const copy = new Survey({
    name: `Copy of ${survey.name}`,
    description: survey.description,
    owner: user._id || user,
  });
  //NOTE Copy questions and criteria separately to get fresh objectIds and timestamps
  for (const question of survey.questions) {
    const criteria = question.criteria.map(criteria => {
      return {
        name: criteria.name, type: criteria.type, minValue: criteria.minValue, maxValue: criteria.maxValue, options: criteria.options
      }
    });

    copy.questions.push({
      name: question.name,
      description: question.description,
      position: question.position,
      criteria,
    });
  }
  if (save) await copy.save();

  return copy;
}

/**
 * @returns {Promise<void>}
 */
schema.methods.generateLinks = async function () {
  if (!this.evaluation) return;
  const evaluation = await Evaluation.findById(this.evaluation._id || this.evaluation).populate('groups');

  this.links = this.links.filter(link => evaluation.groups.some(group => group._id.equals(link.group)));

  for (const group of evaluation.groups) {
    if (!this.links.some(link => link.group.equals(group._id))) this.links.push({ name: group.name, group: group._id, identifier: (Utils.getRandomString(25)) });
  }
}

/**
 * @returns {moment}
 */
schema.methods.getOpeningDate = function () {
  return moment(this.openingDate);
}

/**
 * @returns {moment}
 */
schema.methods.getClosingDate = function () {
  return moment(this.closingDate);
}

/**
 * @param {String|ObjectId|Question} question
 * @returns {void}
 */
schema.methods.moveDown = function (question) {
  question = this.questions.id(question._id || question);
  const previous = this.questions.find(previous => previous.position === (question.position - 1));
  if (!previous) return;
  previous.position++;
  question.position--;

  this.sortQuestions();
}

/**
 * @param {String|ObjectId|Question} question
 * @returns {void}
 */
schema.methods.moveUp = function (question) {
  question = this.questions.id(question._id || question);
  const next = this.questions.find(next => next.position === (question.position + 1));
  if (!next) return;
  next.position--;
  question.position++;

  this.sortQuestions();
}

/**
 * @returns {Boolean}
 */
schema.methods.isOpen = function () {
  const now = moment(new Date());
  const opening = moment(this.openingDate);
  const closing = moment(this.closingDate);

  return (now.isAfter(opening) && now.isBefore(closing)) ? true : false;
}

/**
 * @param {Object} ctx
 * @returns {Promise<Session>}
 */
schema.methods.instantiateSession = async function (ctx) {
  if (!ctx) throw new Error();
  if (!ctx.session.sessions || !ctx.session.sessions instanceof Array) ctx.session.sessions = [];

  const link = this.links.id(ctx.params.link);
  let session;

  //Check if one of the sessions has the current user as owner
  if (ctx.currentUser()) session = this.sessions.find(session => session.owner && session.owner.equals(ctx.currentUser()._id) && link.group.equals(session.group));
  //Check if ctx.session.sessions contains a userless session that belongs to this survey
  if (!session) session = ctx.session.sessions.find(session => this.sessions.some(existing => existing._id.equals(session._id)));

  //If there is no existing session, create new one
  if (!session) {
    const group = await Group.findOne({ _id: link.group });
    this.sessions.push({
      owner: ctx.currentUser() ? ctx.currentUser()._id : null,
      group: group._id,
      group_name: group.name,
    });
    await this.save();
    session = Utils.last(this.sessions);
    ctx.session.sessions.push({ _id: session._id.toString() });
  } else {
    //Otherwise refresh the session using the survey
    session = this.sessions.id(session._id);
  }

  return session;
}

/**
 * @returns {void}
 */
schema.methods.sortQuestions = function () {
  this.questions.sort(function (a, b) {
    return a.position - b.position;
  });

  for (let i = 0; i < this.questions.length; i++) {
    this.questions[i].position = i;
  }
}

/**
 * @param {String|ObjectId} id ObjectId of the question
 * @param {Object} data
 * @returns {Promise<void>}
 */
schema.methods.updateQuestion = async function (id, data) {
  await Survey.updateOne({ _id: this._id, "questions._id": id },
    {
      "$set": { "questions.$": data }
    });
}

/**
 * @param {Object} ctx
 * @param {String} fileName
 * @returns {Promise<String>} fileName
 */
schema.methods.getExcel = async function (ctx, fileName = 'results.xlsx') {
  if (!ctx) throw new Error();
  const workbook = new exceljs.Workbook();
  const worksheet = workbook.addWorksheet();

  const columns = [];
  columns.push({ header: ctx.t('group.title'), key: 'group' });
  for (const question of this.questions) {
    for (const criteria of question.criteria) {
      const column = {
        header: `${criteria.name} (${question.name})`,
        key: `${question._id}-${criteria._id}`
      }
      columns.push(column);
    }
  }
  worksheet.columns = columns;

  for (const session of this.sessions) {
    const row = {};
    for (const column of columns) {
      if (column.key === 'group') {
        row[column.key] = session.group_name;
        continue;
      }

      const question = column.key.split('-')[0];
      const criteria = column.key.split('-')[1];

      const answer = session.answers.find(a => a.question.equals(question));
      if (!answer) continue;

      const subanswer = answer.subanswers.find(a => a.criteria.equals(criteria));
      if (!subanswer) continue;

      if (subanswer.value instanceof Array) {
        row[column.key] = subanswer.value.filter(elem => elem.length).join(', ');
        continue;
      }
      if (typeof subanswer.value !== 'string' && typeof subanswer.value !== 'number') throw new Error();

      row[column.key] = subanswer.value;
    }
  }
  //Style
  worksheet.getRow(1).font = { bold: true };
  worksheet.getRow(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };

  await workbook.xlsx.writeFile(fileName);

  return fileName;
}

/**
 * @param {Object} ctx
 * @returns {Promise<Void>}
 */
schema.methods.notify = async function (ctx) {
  if (!ctx) throw new Error();
  if (!this.evaluation) return;
  await this.generateLinks();
  const evaluation = await Evaluation.findOne({ _id: this.evaluation._id || this.evaluation }).populate('groups');

  for (const group of evaluation.groups) {
    if (group.members.length < 1) continue;

    const link = this.links.find(link => link.group.equals(group._id));
    if (!link) throw new Error();
    const html = `
      <p>${ctx.t('survey.notification.invite', { survey: this.name, opensAt: this.getOpeningDate().format('DD.MM.YYYY'), closesAt: this.getClosingDate().format('DD.MM.YYYY') })}</p>
      <a href="${ctx.getRoute('survey.answer', { id: this._id, link: link._id, identifier: link.identifier }, '', true)}">${ctx.t('survey.notification.link')}</a>`;

    Emailer.sendEmail({ to: group.members.map(member => member.email), html: html });
  }
}

/**
 * @param {String|ObjectId} id
 * @returns {void}
 */
schema.methods.removeQuestion = function(id) {
  this.questions.id(id).remove();

  this.sortQuestions();
}

//Aggregations
/**
 * @returns {Promise<Array<Object>>} Array of Objects constructed by aggregation
 */
schema.methods.getResults = async function () {
  let results = [];
  results = results.concat(await this.getResultsByType('scale'));
  results = results.concat(await this.getResultsByType('select'));
  results = results.concat(await this.getResultsByType('checkbox'));
  results = results.concat(await this.getResultsByType('text'));

  return {
    all: results,
    byQuestion: function (question) {
      if (!question) throw new Error();

      return results.filter(document => document._id.question.equals(question));
    }.bind(this),
    byCriteria: function (criteria, question) {
      if (!criteria) throw new Error();

      return results.filter(document => {
        if (question) return document._id.criteria.equals(criteria) && document._id.question.equals(question);
        return document._id.criteria.equals(criteria);
      });
    }.bind(this),
    byGroup: function (group) {
      if (!group) throw new Error();

      return results.filter(document => document._id.group.equals(group));
    }.bind(this),
  }
}

/**
 * @param {String} type
 * @returns {Promise<Array<Object>>} Array of Objects constructed by aggregation
 */
schema.methods.getResultsByType = async function (type) {
  //TODO additional params like dates...
  let pipeline;
  if (type === 'scale') pipeline = require('./aggregations/results_scale').pipeline;
  else if (type === 'select') pipeline = require('./aggregations/results_select').pipeline;
  else if (type === 'checkbox') pipeline = require('./aggregations/results_checkbox').pipeline;
  else if (type === 'text') pipeline = require('./aggregations/results_text').pipeline;
  else throw new Error(`Expected a type. Found: ${type}`);

  return await Survey.aggregate(pipeline(this._id));
};

exports.model = { name: 'Survey', schema };