const mongoose = require('mongoose');

/**
 * Schema: Criteria
 */
const schema = mongoose.Schema({
  name: { type: String, required: true, default: 'New Criteria' },
  type: { type: String, required: true, default: 'scale' },

  // Scale options
  minValue: { type: Number },
  maxValue: { type: Number },

  // Select
  options: [String],
});

schema.statics.types = function () {
  return ['scale', 'select', 'checkbox', 'text'];
}

exports.model = { name: 'Criteria', schema, subdocument: true }