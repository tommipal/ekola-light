const mongoose = require('mongoose');

const subAnswersChema = mongoose.Schema({
  criteria: { type: mongoose.Schema.Types.ObjectId, required: true },
  name: { type: String, required: true },
  type: { type: String, required: true },
  value: { type: mongoose.Schema.Types.Mixed, required: true }
});
subAnswersChema.pre('save', function () {
  if (!isNaN(this.value)) this.value = parseInt(this.value);
});

/**
 * Schema: Session
 */
const schema = mongoose.Schema({
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },
  group: { type: mongoose.Schema.Types.ObjectId, ref: 'Group', required: true },
  group_name: { type: String, required: true},
  open: { type: Boolean, required: true, default: true },
  answers: [mongoose.Schema({
    question: { type: mongoose.Schema.Types.ObjectId, required: true },
    name: { type: String, required: true },
    subanswers: [subAnswersChema]
  })]
}, { timestamps: {} });

exports.model = { name: 'Session', schema, subdocument: true };