const moment = require('moment');

class Authenticator {
  /**
   *
   */
  static async evaluation(ctx, next) {
    let evaluation;
    if (ctx.request.body.evaluation !== undefined) evaluation = await Evaluation.findById(ctx.request.body.evaluation);
    else evaluation = await Evaluation.findById(ctx.params.id);

    if (!evaluation) {
      ctx.getFlashMessanger().showAlert('Could not find the requested evaluation');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }
    if (evaluation.owner.toString() !== ctx.currentUser()._id)
      throw new Error(`EvaluationController: User ${ctx.currentUser._id} isn't allowed to modify evaluation ${ctx.params.id}`);

    return await next();
  }

  /**
   *
   */
  static async group(ctx, next) {
    let group;
    if (ctx.request.body.group !== undefined) group = await Group.findById(ctx.request.body.group);
    else group = await Group.findById(ctx.params.id);

    if (!group) {
      ctx.getFlashMessanger().showAlert('Could not find the requested group');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }
    if (group.owner.toString() !== ctx.currentUser()._id)
      throw new Error(`GroupController: User ${ctx.currentUser._id} isn't allowed to modify group ${ctx.params.id}`);

    if (ctx.params.member === undefined) return await next();
    const member = group.members.id(ctx.params.member);
    if (!member) {
      ctx.getFlashMessanger().showAlert('Could not find the requested member');
      return ctx.redirect(ctx.getRoute('group.edit', { id: group._id }));
    }

    return await next();
  }

  /**
   * @param {*} ctx
   * @param {*} next
   */
  static async survey(ctx, next) {
    let survey;
    if (ctx.request.body.survey !== undefined) survey = await Survey.findById(ctx.request.body.survey);
    else survey = await Survey.findOne({ _id: ctx.params.id });

    if (!survey) {
      ctx.getFlashMessanger().showAlert('error.survey.notFound');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }
    if (survey.owner.toString() !== ctx.currentUser()._id) {
      console.error(`SurveyController: User ${ctx.currentUser._id} isn't allowed to modify survey ${ctx.params.id}`);
      ctx.getFlashMessanger().showAlert('error.forbidden');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }

    if (!ctx.params.question && ctx.request.body.question === undefined) return await next();
    const question = survey.questions.id(ctx.request.body.question ? ctx.request.body.question : ctx.params.question);
    if (!question) {
      ctx.getFlashMessanger().showAlert('error.question.notFound');
      return ctx.redirect(ctx.getRoute('survey.edit', { id: survey._id }));
    }

    if (!ctx.params.criteria && ctx.request.body.criteria === undefined) return await next();
    const criteria = question.criteria.id(ctx.params.criteria ? ctx.params.criteria : ctx.request.body.criteria);
    if (!criteria) {
      ctx.getFlashMessanger().showAlert('error.criteria.notFound');
      return ctx.redirect(ctx.getRoute('survey.questions.edit', { id: survey._id, question: question._id }));
    }

    return await next();
  }

  /**
   *
   */
  static async survey_public(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    if (!survey) {
      ctx.getFlashMessanger().showAlert('error.survey.notFound');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }
    if (!survey.public && !survey.owner.equals(ctx.currentUser()._id)) {
      ctx.getFlashMessanger().showAlert('error.survey.notPublic');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }

    return await next();
  }

  static async verifyLink(ctx, next) {
    const backupUrl = ctx.getRoute('app.index');

    const survey = await Survey.findOne({ _id: ctx.params.id });
    if (!survey) {
      ctx.getFlashMessanger().showAlert('Could now find the requested survey');
      return ctx.redirect(backupUrl);
    }

    if (!survey.isOpen()) return ctx.view('survey/closed', { survey, moment });

    const link = survey.links.id(ctx.params.link);
    if (!link) {
      ctx.getFlashMessanger().showAlert('error.linkNotFound');
      return ctx.redirect(backupUrl);
    }
    if (link.identifier !== ctx.params.identifier) {
      console.warn(`Identifier mismatch! Expected: ${link.identifier} but instead got ${ctx.params.identifier}`);
      ctx.getFlashMessanger().showAlert('error.linkNotFound');
      return ctx.redirect(backupUrl);
    }
    const group = await Group.findOne({ _id: link.group });
    if (!group) {
      console.warn(`Link ${link._id} points to group ${link.group} that doesn't seem to exist anymore!`);
      ctx.getFlashMessanger().showAlert('error.linkNotValid');
      await survey.generateLinks();
      return ctx.redirect(backupUrl);
    }

    return await next();
  }

  /**
   * @param {Object} ctx
   * @param {Function} next
   */
  static async login(ctx, next) {
    if (!ctx.session.user) {
      console.info('Authenticator.login: Unauthorized user. Redirecting to login');
      return ctx.redirect(ctx.getRoute('user.login'));
    }
    if (await User.findOne({ _id: ctx.session.user._id }) === null) {
      console.info(`Authenticator.login: User ${ctx.session.user._id} doesn't seem to exist anymore. Redirecting to login`);
      ctx.session.user = null;
      return ctx.redirect(ctx.getRoute('user.login'));
    }

    return await next();
  }

  /**
   * @param {Object} ctx
   * @param {Function} next
   */
  static async isLoggedin(ctx, next) {
    if (ctx.session.user) {
      console.info('Authenticator.isLoggedin: User is already loggedin. Redirecting to profile')
      return ctx.redirect(ctx.getRoute('user.profile'));
    }

    return await next();
  }

}

module.exports = Authenticator;