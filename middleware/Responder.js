const { config } = require('common');

class Responder {
  /**
   * Function as an outer try catch to handle any errors
   * @param {Object} ctx
   * @param {Function} next
   */
  static async handle(ctx, next) {
    try {
      await next();
    } catch (err) {
      console.error(err);
      if (config.env === 'development') return ctx.view('error', { error: err });
      return ctx.view('error_production', { error: err });
    }
  }
}

module.exports = Responder;