import Chart from 'chart.js';
import Translator from 'translator.js';
const t = Translator.Translator;

class AnswerDistributionVisualizer {
  constructor() { }
  get backgroundColors() {
    return ['rgba(255, 99, 132, 1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(70, 252, 107, 1)',
    'rgba(143, 11, 145, 1)',
    'rgba(31, 145, 126, 1)',
    'rgba(237, 140, 14, 1)',
    'rgba(219, 59, 133, 1)',
    'rgba(50, 81, 130, 1)',
    ];
  }

  async init() {
    const locale = document.getElementById('data').getAttribute('data-locale');
    const params = document.getElementById('params');
    const url = params.getAttribute('data-url');

    const response = await fetch(url, {
      credentials: "same-origin"
    });
    const json = await response.json();
    const evaluation = json.evaluation;
    const survey = json.survey;

    let labels = [];
    let data = [];
    if (!evaluation || evaluation.groups.length < 1) {
      const groups = [... new Set(survey.sessions.map(session => session.group))];
      if (!groups.length) return;
      for (const id of groups) {
        labels.push(survey.sessions.find(session => session.group === id).group_name);
        data.push(survey.sessions.filter(session => session.group === id).length);
      }
    } else {
      labels = evaluation.groups.map(group => group.name);
      data = evaluation.groups.map(group => {
        return survey.sessions.filter(session => session.group === group._id).length;
      });
    }
    new Chart(document.getElementById("results-canvas"), {
      type: 'pie',
      data: {
        labels: labels,
        datasets: [{
          backgroundColor: this.backgroundColors,
          data: data,
        }]
      },
      options: {
        title: {
          display: true,
          text: t.translate('survey.results.numberOfAnswers', locale),
          responsive: true,
          maintainAspectRatio: false,
        }
      }
    });
  }
}


$(document).ready(function () {
  const app = new AnswerDistributionVisualizer();
  app.init();
});