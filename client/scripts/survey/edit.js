import Translator from 'translator.js';
const t = Translator.Translator;

$(document).ready(function () {
  const locale = document.getElementById('data').getAttribute('data-locale');

  for (const button of $('.question-delete-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-question');
      $('#question-delete-modal > form > input[name=question]').val(value);

      $('#question-delete-modal').foundation('open');
    });
  }

  for (const button of $('.copy-link-button')) {
    button.addEventListener('click', function(e) {
     const link = e.target.getAttribute('data-link');
     const input = $(`#${link}`)[0];

     input.select();
     document.execCommand("copy");

     window.alert(t.translate('survey.edit.linkCopied', locale, { link: input.value }));
    });
  }

  document.getElementById('send-notification-button').addEventListener('click', function (e) {
    const url = e.target.getAttribute('data-url');

    $('#common-modal-message').text(t.translate('survey.edit.notifyWarning', locale));
    $('#common-modal-confirm').attr('href', url);
    $('#common-modal').foundation('open');
  });
});