import Chart from 'chart.js';
import Translator from 'translator.js';
const T = Translator.Translator;

class ResultsManager {
  constructor() {
    this.locale = document.getElementById('data').getAttribute('data-locale');
  }

  get backgroundColors() {
    return ['rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(70, 252, 107, 0.2)',
      'rgba(143, 11, 145, 0.2)',
      'rgba(31, 145, 126, 0.2)',
      'rgba(237, 140, 14, 0.2)',
      'rgba(219, 59, 133, 0.2)',
      'rgba(50, 81, 130, 0.2)',
    ];
  }
  get borderColors() {
    return ['rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(70, 252, 107, 1)',
      'rgba(143, 11, 145, 1)',
      'rgba(31, 145, 126, 1)',
      'rgba(237, 140, 14, 1)',
      'rgba(219, 59, 133, 1)',
      'rgba(50, 81, 130, 1)',
    ];
  }

  async init() {
    const params = document.getElementById('params');
    const url = params.getAttribute('data-url');

    const response = await fetch(url, {
      credentials: "same-origin"
    });
    const json = await response.json();

    this.results = json.results;
    this.survey = json.survey;
    let groups = [...new Set(this.results.map(elem => elem._id.group))];
    if (json.groups && json.groups.length) groups = groups.filter(group => json.groups.some(g => g._id === group));

    // Append some contents to the info div
    const infoContainer = `#survey-info`;

    $(infoContainer).append(`
      <label>${T.translate('survey.results.numberOfAnswers', this.locale)}: <b>${this.survey.sessions.length}</b>
        <ul>
          ${groups.map(group => {
            const sessions = this.survey.sessions.filter(session => session.group === group);
            const groupName = sessions[0].group_name;
            return `<li>${groupName}: <b>${sessions.length}</b></li>`
          }).join(' ')}
        </ul>
      </label>`);

    for (const question of this.survey.questions) {
      for (const criteria of question.criteria) {
        const resultSet = this.results.filter(res => res._id.question === question._id && res._id.criteria === criteria._id);
        const ctx = document.getElementById(`${question._id}-${criteria._id}`);

        if (criteria.type === 'scale') {
          this.drawChartForScale(ctx, resultSet.map(doc => doc.average), resultSet.map(doc => doc._id.group_name));

        } else if (criteria.type === 'select' || criteria.type === 'checkbox') {
          this.drawChartForSelect(ctx, resultSet.filter(doc => doc._id.value !== null), [...criteria.options]);

        } else if (criteria.type === 'text') {
          ctx.parentElement.parentElement.removeChild(ctx.parentElement);

          const container = `#${question._id}-${criteria._id}-container`;

          for (const answer of resultSet) {
            $(container).append(`
              <fieldset class="fieldset">
                <legend>
                  <h5>${answer._id.group_name}</h5>
                </legend>
                ${answer.values.map(answer => `<p>"${answer}"</p>`).join(' ')}
              </fieldset>`);
          }
        }
      }
    }
  }

  /**
   *
   */
  drawChartForScale(ctx, data = [], labels = []) {
    if (!ctx) throw new Error();

    const chart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          label: T.translate('survey.results.averageValue', this.locale),
          data: data,
          backgroundColor: this.backgroundColors,
          borderColor: this.borderColors,
          borderWidth: 1
        }]
      },
      options: {
        legend: { display: false },
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    return chart;
  }

  /**
   *
   */
  drawChartForSelect(ctx, data = [], options) {
    const datasets = [];

    const groups = [...new Set(data.map(elem => elem._id.group))];

    const colors = this.backgroundColors;
    const borderColors = this.borderColors;

    for (const group of groups) {
      const documents = data.filter(doc => doc._id.group === group);
      const obj = {
        label: documents[0]._id.group_name,
        data: [],
        backgroundColor: colors.shift(),
        borderColor: borderColors.shift(),
        borderWidth: 1
      }
      for (const option of options) {
        let elem = documents.find(cc => cc._id.value === option);
        obj.data.push(elem ? elem.count : 0);
      }
      datasets.push(obj)
    }

    var data = {
      labels: options.map(option => option.length > 20 ? option.slice(0, 20) + '...' : option),
      datasets: datasets
    };

    const chart = new Chart(ctx, {
      type: 'bar',
      data,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    return chart;
  }
}

function bindEventListeners() {
  //Button for collapsing all the accordion tabs (containing answers)
  document.getElementById('collapse-all-button').addEventListener('click', function (e) {
    const accordion = $('.accordion');
    const tabsToClose = accordion.find('.accordion-item.is-active .accordion-content');

    tabsToClose.each(function (i, tab) {
      accordion.foundation('up', $(tab));
    });
  });
}

$(document).ready(function () {
  bindEventListeners();

  const app = new ResultsManager();
  app.init();
});