$(document).ready(function () {
  for (const button of $('.criteria-delete-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-criteria');
      $('#criteria-delete-modal > form > input[name=criteria]').val(value);

      $('#criteria-delete-modal').foundation('open');
    });
  }
});