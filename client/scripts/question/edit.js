$(document).ready(function () {
  for (const button of $('.question-delete-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-question');
      $('#question-delete-modal > form > input[name=question]').val(value);

      $('#question-delete-modal').foundation('open');
    });
  }

  for (const button of $('.criteria-delete-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-criteria');
      $('#criteria-delete-modal > form > input[name=criteria]').val(value);

      $('#criteria-delete-modal').foundation('open');
    });
  }
});
