$(document).ready(function() {
  for (const button of $('.promote-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-user');
      $('#promote-modal > form > input[name=user]').val(value);

      $('#promote-modal').foundation('open');
    });
  }

  for (const button of $('.demote-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-user');
      $('#demote-modal > form > input[name=user]').val(value);

      $('#demote-modal').foundation('open');
    });
  }
});
