$(document).ready(function () {
  for (const button of $('.survey-delete-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-survey');
      $('#survey-delete-modal > form > input[name=survey]').val(value);

      $('#survey-delete-modal').foundation('open');
    });
  }

  for (const button of $('.evaluation-delete-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-evaluation');
      $('#evaluation-delete-modal > form > input[name=evaluation]').val(value);

      $('#evaluation-delete-modal').foundation('open');
    });
  }

  for (const button of $('.group-delete-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-group');
      $('#group-delete-modal > form > input[name=group]').val(value);

      $('#group-delete-modal').foundation('open');
    });
  }
});