$(document).ready(function() {
  for (const button of $('.verify-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-user');
      $('#verify-modal > form > input[name=user]').val(value);

      $('#verify-modal').foundation('open');
    });
  }

  for (const button of $('.reject-button')) {
    button.addEventListener('click', e => {
      const value = e.target.getAttribute('data-user');
      $('#reject-modal > form > input[name=user]').val(value);

      $('#reject-modal').foundation('open');
    });
  }
});
