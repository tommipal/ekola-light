# Ekola - Web application for managing surveys
## Requirements
- Node.js >= 9
- MongoDB >= 3.6

## Database
- It's recommended to create a new MongoDB database for the application.
- NOTE! Database user is authenticated using the app's database and not the admin database.
- Adding new user (example):
```
Use ekola
db.createUser({ user: "ekola", pwd: "mahdottomanvaikeasalasana",
  roles: [ { role: "dbAdmin", db: "ekola" }, { role: "readWrite", db: "ekola" } ]
})
```
- Remember to enable authentication in the configuration (mongod.cfg):
```
security:
  authorization: enabled
```

## Required environment variables
- Environment:
  - NODE_ENV (production / development)
- Database:
  - EKOLA_DATABASE_HOST
  - EKOLA_DATABASE
  - EKOLA_DATABASE_USER
  - EKOLA_DATABASE_PASSWORD
- Cookies:
  - EKOLA_SESSION_SECRET
- Admin user:
  - EKOLA_ADMIN_EMAIL
  - EKOLA_ADMIN_PASSWORD
- See Wiki for additional variables

## Setup / Scripts
- Install packages:
  - npm install

### Production environment
- Compile assets:
  - npm run build
- Import QAEMP questions
  - npm run import-qaemp
- Start the application in production mode
  - npm run start-prod
### Development environment
- Run tests
  - npm run test
- Generate test data. (Quantity and properties can be modified in tests/scripts/populate.js)
  - npm run populate
- Start the application in development mode
  - npm run start
### General
- Grant admin privileges to user
  - npm run add-admin -- --email:test.user@email.fi