const moment = require('moment');
const { Form } = require('forms');

class SurveyForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx, object) {
    super(ctx, object);
    this.model = Survey;

    this.fields = [
      new this._fields.TextField({
        label:this.t('common.name'),
        property: 'name',
        type: 'text',
        placeholder: this.t('common.name'),
        required: true
      }),
      new this._fields.TextareaField({
        label: this.t('common.description'),
        property: 'description',
        type: 'textarea',
        placeholder: this.t('common.description'),
        rows: 3,
        required: false,
      }),
      new this._fields.SelectField({
        label: this.t('survey.evaluation'),
        property: 'evaluation',
        type: 'select',
        options: [],
        optionDisplayProperty: 'name',
        optionValueProperty: '_id',
        required: false,
      }),
      new this._fields.DateField({
        label: this.t('survey.dates.openingDate'),
        property: 'openingDate',
        type: 'date',
        placeholder: moment(Date.now()).subtract(1, 'days').format('YYYY-MM-DD'),
        required: true,
      }),
      new this._fields.DateField({
        label: this.t('survey.dates.closingDate'),
        property: 'closingDate',
        type: 'date',
        placeholder: moment(Date.now()).subtract(1, 'days').format('YYYY-MM-DD'),
        required: true,
      }),
      new this._fields.SelectField({
        label: this.t('survey.is.public'),
        property: 'public',
        options: [{ value: true, title: this.t('common.yes')}, {value: false, title: this.t('common.no')}],
        optionValueProperty: 'value',
        optionDisplayProperty: 'title',
      }),
    ];
  }

  static async build(ctx, object) {
    const form = new SurveyForm(ctx, object);
    const evaluations = await Evaluation.find({ owner: ctx.currentUser()._id, archived: false });

    form.fields.find(field => field.property === 'evaluation').options = evaluations;
    form.populate();

    return form;
  }
}

module.exports = SurveyForm;