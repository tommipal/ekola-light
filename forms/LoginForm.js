const { Form } = require('forms');

class LoginForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx) {
    super(ctx);

    this.submit_message = 'user.login';

    this.fields = [
      new this._fields.TextField({
        label: this.t('common.email'),
        property: 'email',
        type: 'email',
        placeholder: this.t('common.email'),
        required: true,
      }),
      new this._fields.TextField({
        label: this.t('user.password'),
        property: 'password',
        type: 'password',
        placeholder: this.t('user.password'),
        required: true
      })];

    this.validation.push(async function () {
      const email = this.property('email');
      const password = this.property('password');
      const match = await User.match(email, password);

      const verified = await User.isVerified({ email });
      if (match === false) this.showError('error.invalidCredentials');
      else if (verified === false) {
        this.showError('error.userNotVerified');
        return false;
      }

      return match
    }.bind(this));

    this.populate();
  }

  /**
   * @override
   * @returns {Promise<User>} User
   */
  async getData() {
    const email = this.property('email');

    return await User.findOne({ email: email });
  }
}

module.exports = LoginForm;