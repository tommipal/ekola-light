const { Form } = require('forms');

class UserForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx) {
    super(ctx);
    this.submit_message = 'user.register';

    this.fields = [
      new this._fields.TextField({
        label: this.t('common.email'),
        property: 'email',
        type: 'email',
        placeholder: this.t('common.email'),
        required: true,
        validate: async function () {
          if (!this.value) return false;
          if (await User.findOne({ email: this.value })) {
            console.info(`UserForm: Email '${this.value}' is already reserved`);
            ctx.getFlashMessanger().showAlert('errors.emailReserved');
            return false;
          }
          return true;
        }
      }),
      new this._fields.TextField({
        label: this.t('user.password'),
        property: 'password',
        type: 'password',
        placeholder: this.t('user.password'),
        required: true,
        minLength: 4,
      }),
      new this._fields.TextField({
        label: this.t('user.confirmPassword'),
        property: 'confirmation',
        type: 'password',
        placeholder: this.t('user.confirmPassword'),
        required: true,
        minLength: 4,
      })
    ]

    this.validation.push(function () {
      const match = (this.property('password') === this.property('confirmation'));
      if (!match) this.showError(`errors.register.password_missmatch`);

      return match;
    }.bind(this));

    this.populate();
  }

  /**
   * @returns {Promise<User>}
   */
  async getData() {
    const data = {};
    for (const field of this.fields) {
      switch (field.property) {
        case 'password':
          const encrypted = await User.hashpwd(field.value);
          data.password = encrypted.encrypted;
          data.iv = encrypted.iv;
          break;
        default:
          data[field.property] = field.value;
      }
    }
    const user = new User(data);
    try {
      await user.save();
    } catch (err) {
      console.error(err);
      return null;
    }

    return user;
  }
}

module.exports = UserForm;