const { Form } = require('forms');
const { Utils } = require('common');

class AnswerForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx, object) {
    super(ctx, object);

    this.fields = [];
  }

  scale(criteria, question) {
    const field = new this._fields.SliderField({
      label: criteria.name,
      property: `${question._id}-${criteria._id}`,
      type: 'slider',
      required: false,
      min: criteria.minValue,
      max: criteria.maxValue,
    });

    this.fields.push(field);
  }
  select(criteria, question) {
    const field = new this._fields.SelectField({
      label: criteria.name,
      property: `${question._id}-${criteria._id}`,
      options: criteria.options,
      type: 'select',
      required: false,
    });

    this.fields.push(field);
  }
  text(criteria, question) {
    const field = new this._fields.TextareaField({
      label: criteria.name,
      property: `${question._id}-${criteria._id}`,
      rows: 3,
      required: false
    });

    this.fields.push(field);
  }
  checkbox(criteria, question) {
    const field = new this._fields.CheckboxField({
      label: criteria.name,
      property: `${question._id}-${criteria._id}`,
      type: 'checkbox',
      options: criteria.options,
    });
    this.fields.push(field);
  }

  /**
   * Define the fields here based on the survey's questions.
   * Populate the field first using the answers and then using the request
   */
  static async build(ctx, survey, session) {
    const form = new AnswerForm(ctx, survey);

    survey.questions.forEach(question => {
      question.criteria.forEach(criteria => {
        switch (criteria.type) {
          case 'scale':
            form.scale(criteria, question);
            break;
          case 'select':
            form.select(criteria, question);
            break;
          case 'text':
            form.text(criteria, question);
            break;
          case 'checkbox':
            form.checkbox(criteria, question);
            break;
        }
      });
    });

    form.prePopulate(session);
    form.populate();

    return form;
  }

  /**
   * Prepopulate form using existing answers
   */
  prePopulate(session) {
    for (const field of this.fields) {
      let question = field.property.split('-')[0];
      let criteria = field.property.split('-')[1];

      const answer = session.answers.find(answer => answer.question.equals(question));
      if (!answer) continue;

      const subanswer = answer.subanswers.find(subanswer => subanswer.criteria.equals(criteria));
      if (!subanswer) continue;

      if (field.populate) field.value = field.populate(subanswer.value);
      else field.value = subanswer.value;
    }
  }

  /**
   * @override
   */
  async getData(survey, session) {
    session = survey.sessions.id(session._id);

    // Filter out fields without a value
    const fields = this.fields.filter(field => {
      const value = field.getDataRaw();
      return (value === NaN || value === undefined || value === null) ? false : true;
    });

    for (const field of fields) {
      let questionId = field.property.split('-')[0];
      const question = survey.questions.id(questionId);
      let criteriaId = field.property.split('-')[1];
      const criteria = question.criteria.id(criteriaId);

      //Check that the question and criteria are part of this survey
      const questionObject = survey.questions.find(q => q._id.equals(questionId));
      if (!questionObject) continue;
      const criteriaObject = questionObject.criteria.find(c => c._id.equals(criteriaId));
      if (!criteriaObject) continue;

      //Init answer (if needed)
      let answer = session.answers.find(answer => answer.question.equals(question._id));
      if (!answer) {
        session.answers.push({
          question: question._id,
        });
        answer = Utils.last(session.answers);
      }

      //Init subanswer (if needed)
      let subanswer = answer.subanswers.find(answer => answer.criteria.equals(criteria._id));
      if (!subanswer) {
        answer.subanswers.push({
          criteria: criteria._id,
          name: criteriaObject.name,
          type: criteriaObject.type
        });
        subanswer = Utils.last(answer.subanswers);
      }

      //Update subanswer's value
      subanswer.value = field.getDataRaw();

      //Insert/update info
      answer.name = question.name;
      subanswer.name = criteria.name;
    };
    await survey.save();
  }
}

module.exports = AnswerForm;