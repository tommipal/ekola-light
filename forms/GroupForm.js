const { Form } = require('forms');

class GroupForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx, object) {
    super(ctx, object);
    this.model = Group;

    this.fields = [
      new this._fields.TextField({
        label: this.t('common.name'),
        property: 'name',
        type: 'text',
        placeholder: this.t('common.name'),
        required: true
      }),
      new this._fields.TextareaField({
        label: this.t('common.description'),
        property: 'description',
        type: 'textarea',
        placeholder: this.t('common.description'),
        rows: 3,
        required: false,
      }),
    ];
  }

  static async build(ctx, object) {
    const form = new GroupForm(ctx, object);
    form.populate();

    return form;
  }
}

module.exports = GroupForm;