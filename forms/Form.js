const { Utils } = require('common');

const { CheckboxField } = require('./fields/CheckboxField');
const { DateField } = require('./fields/DateField');
const { NumberField } = require('./fields/NumberField');
const { SelectField } = require('./fields/SelectField');
const { SliderField } = require('./fields/SliderField');
const { TextareaField } = require('./fields/TextareaField');
const { TextField } = require('./fields/TextField');

class Form {
  constructor(ctx, object) {
    if (!ctx) throw new Error();
    this.ctx = ctx;
    this.object = object;
    this.model = null;

    this.fields = [];
    this.validation = [];

    this.id = Utils.getRandomString(10);
    this.route;
    this.renderedStart = false;
    this.renderedEnd = false;
    this.renderedSubmit = false;

    this.submit_message = 'common.submit';
  }

  static build() {
    throw new Error('Implement me!');
  }

  data() {
    return {
      get: function (object) {
        for (const field of this.fields) field.getData(object);

        return object;
      }.bind(this),
      set: function (object) {
        for (let [name, val] of Utils.iterateObject(object)) {
          const field = this.fields.find(field => field.property === name);

          if (field) {
            if (field.populate) field.value = field.populate(val);
            else field.value = val;
          }
        }
      }.bind(this),
    }
  }

  /**
   * @param {String} property
   * @returns {Field} field
   */
  findField(property) {
    return this.fields.find(field => field.property === property);
  }

  get _fields() {
    return {
      CheckboxField, DateField, NumberField, SelectField, SliderField, TextareaField, TextField
    }
  }
  static get _fields() {
    return {
      CheckboxField, DateField, NumberField, SelectField, SliderField, TextareaField, TextField
    }
  }

  /**
   * Returns the data stored in the form as an object.
   * @returns {Object}
   */
  getData() {
    //Use the object tied to the form if possible
    if (this.object) {
      this.data().get(this.object);

      return this.object;
    }
    //Otherwise return new object
    const data = {};
    this.data().get(data);

    return this.model ? new this.model(data) : data;
  }

  /**
   * Run all validation related to the form
   * @returns {Boolean} isValid
   */
  async isValid() {
    if (this.ctx.method !== 'POST') return false;
    let isValid = true;
    for (const field of this.fields) {
      if (!field.value && field.required) {
        isValid = false;
      }
      if (field.validate) {
        if (!(field.validate instanceof Function)) throw new Error();
        let result = field.validate();
        if (result instanceof Promise) result = await result;
        if (result !== true) isValid = false;
      }
      if (field.minLength && new String(field.value).length < field.minLength) {
        this.showError('Minimum length not met!');
        isValid = false;
      }
      if (field.maxLength && new String(field.value).length > field.maxLength) {
        this.showError(`Maximum length not met!`);
        isValid = false;
      }
    }
    if (await this.validate() === false) isValid = false;

    return isValid;
  }

  populate() {
    if (this.object) this.data().set(this.object._doc || this.object);
    this.data().set(this.ctx.request.body);
  }

  /**
   * @param {String} property
   * @returns {any} value
   */
  property(property) {
    const field = this.fields.find(field => field.property === property);
    if (!field) throw new Error();

    return field.value;
  }

  /**
   * Wrapper for the error messaging implementation
   * @param {String} mesasge
   * @param {String} title
   */
  showError(message, title) {
    this.ctx.getFlashMessanger().showAlert(message, title);
  }

  /**
   * @param {String} route
   * @param {Object} params
   */
  setRoute(route, params = {}) {
    this.route = this.ctx.getRoute(route, params);
  }

  /**
   * @param {String} property
   * @returns {String} HTML
   */
  render(property) {
    if (!this.renderedStart) throw new Error('form.renderStart() should always be called before rendering fields!');

    var field = this.fields.find(field => field.property === property);

    if (!field) throw new Error(`form.render(): Could not find field for property ${property}`);

    return field.render();
  }

  /**
   * @returns {String} HTML
   */
  renderEnd() {
    if (this.renderedEnd) return '';

    this.renderedEnd = true;
    return `</form>`;
  }

  /**
   * @returns {String} HTML
   */
  renderFields() {
    if (!this.renderedStart) throw new Error('form.renderStart() should always be called before rendering fields!');

    return this.fields.map(field => field.render()).join(' ')
  }

  /**
  * @returns {String} HTML
  */
  renderStart() {
    if (!this.route) throw new Error(`Missing route for submit!`);
    if (this.renderedStart) return '';

    this.renderedStart = true;
    let start = `<form action="${this.route}" id="${this.id}" method="POST">`;
    start += `<input type="hidden" name="_csrf" value="${this.ctx.csrf}">`;

    return start;
  }

  /**
   * @returns {String} HTML
   */
  renderSubmit() {
    if (this.renderedSubmit) return '';

    this.renderedSubmit = true;
    let elem = `<button type="submit" class="button"><i class="fi-check"></i> ${this.t(this.submit_message)}</button>`;

    return elem;
  }

  /**
   * Returns HTML contains all the form elemets that have yet to be rendered
   * @returns {String} HTML
   */
  renderRest() {
    const html = this.renderStart() + this.fields.map(field => field.render()).join(' ') + this.renderSubmit() + this.renderEnd();

    return html;
  }

  /**
   * Wrapper for translation function
   * @param {String} key
   */
  t(key) {
    return this.ctx.t(key);
  }

  /**
  * Run the validation functions of the form itself
  * @returns {Boolean} isValid
  */
  async validate() {
    if (this.validation.length === 0) return true;
    if (!this.validation.every(validationFunction => validationFunction instanceof Function)) throw new Error();

    let isValid = true;
    for (const validationFunction of this.validation) {
      let result = validationFunction();
      if (result instanceof Promise) result = await result;
      if (result !== true) isValid = false;
    }

    return isValid;
  }

}

module.exports = Form;