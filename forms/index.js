exports.Form = require('./Form');

exports.AnswerForm = require('./AnswerForm');
exports.CriteriaForm = require('./CriteriaForm');
exports.EvaluationForm = require('./EvaluationForm');
exports.GroupForm = require('./GroupForm');
exports.GroupMemberForm = require('./GroupMemberForm');
exports.LoginForm = require('./LoginForm');
exports.QuestionForm = require('./QuestionForm');
exports.SurveyForm = require('./SurveyForm');
exports.UserForm = require('./UserForm');