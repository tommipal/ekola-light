const { Form } = require('forms');

class GroupMemberForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx, object) {
    super(ctx, object);

    this.fields = [
      new this._fields.TextField({
        label: this.t('common.email'),
        property: 'email',
        type: 'email',
        placeholder: this.t('common.email'),
        required: true
      }),
      new this._fields.TextField({
        label: this.t('common.name'),
        property: 'name',
        type: 'text',
        placeholder: this.t('common.name'),
        required: false,
      }),
      new this._fields.TextField({
        label: this.t('common.description'),
        property: 'description',
        type: 'text',
        placeholder: this.t('common.description'),
        required: false,
      })];
  }

  static async build(ctx, object) {
    const form = new GroupMemberForm(ctx, object);
    form.populate();

    return form;
  }
}

module.exports = GroupMemberForm;