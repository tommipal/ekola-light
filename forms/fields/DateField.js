const { Field } = require('./Field');
const moment = require('moment');

class DateField extends Field {
  constructor({ property, required = false, label, value, defaultValue } = {}) {
    super({ property, required, label, value });
    this.defaultValue = defaultValue;
  }

  /**
   * @override
   */
  getData(object) {
    const value = moment(this.value, 'YYYY-MM-DD').toDate();
    object[this.property] = value;
  }

  /**
   * @override
   * @returns {Date}
   */
  getDataRaw() {
    return moment(this.value, 'YYYY-MM-DD').toDate();
  }

  /**
   * @override
   * @returns {String} HTML
   */
  render() {
    if (this.rendered) return '';
    this.rendered = true;

    let value;
    if (this.value || this.defaultValue) value = moment(this.value || this.defaultValue).format('YYYY-MM-DD');

    let element = `<input name="${this.property}" type="date" ${value ? `value="${value}"` : ''}} >`;
    if (this.label) element = `<label>${this.label} ${element}</label>`;

    return element;
  }
}

exports.DateField = DateField;