const { Field } = require('./Field');
const { Utils } = require('common');

class TextField extends Field {
  constructor({ property, required = false, label, value, placeholder, type = 'text', validate, extractData, populate, minLength } = {}) {
    super({ property, required, placeholder, label, value, validate, extractData, populate });
    this.type = type;
    if (minLength) this.minLength = minLength;
  }

  sanitizeData() {
    this.value = Utils.sanitizeString(this.value);
  }

  /**
   * @override
   * @returns {String} HTML
   */
  render() {
    if (this.rendered) return '';
    this.rendered = true;


    let element = `<input
      type="${this.type}"
      ${this.render_name}
      ${this.render_value}
      ${this.render_placeholder}
      ${this.render_required}
      ${this.minLength ? `minlength="${this.minLength}"` : ''}>`;

    element = this.render_label(element);

    return element
  }
}

exports.TextField = TextField;