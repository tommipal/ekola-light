const { Field } = require('./Field');
const { Utils } = require('common');

class TextareaField extends Field {
  constructor({ property, required = false, label, value, rows = 3, placeholder, populate, extractData } = {}) {
    super({ property, required, placeholder, label, value, populate, extractData });
    this.rows = rows;
  }

  sanitizeData() {
    this.value = Utils.sanitizeString(this.value);
  }

  /**
   * @override
   * @returns {String} HTML
   */
  render() {
    if (this.rendered) return '';
    this.rendered = true;

    let element = `<textarea ${this.render_name} rows="${this.rows}" ${this.render_placeholder}>`;
    element += this.value || '';
    element += `</textarea>`;
    element = this.render_label(element);

    return element
  }
}

exports.TextareaField = TextareaField;