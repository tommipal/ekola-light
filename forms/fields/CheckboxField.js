const { Field } = require('./Field');

class CheckboxField extends Field {
  constructor({ property, required = false, label, value, options = [], optionDisplayProperty, optionValueProperty, extractData, populate } = {}) {
    super({ property, required, label, value, extractData, populate });

    this.options = options;
    this.optionDisplayProperty = optionDisplayProperty;
    this.optionValueProperty = optionValueProperty;
  }

  /**
   * @override
   * @returns {String} HTML
   */
  render() {
    if (this.rendered) return '';
    this.rendered = true;
    if (!(this.options instanceof Array)) throw new Error();

    if (this.value && !(this.value instanceof Array)) this.value = [this.value];

    let element;
    if (this.optionDisplayProperty && this.optionValueProperty) {
      element = this.options.map((option, i) => {
        let e = `<input type="checkbox"
          ${this.render_name}
          value="${option[this.optionValueProperty]}"
          id="${this.property}_label_${i}"
          ${this.isChecked(option)}>`;

        e += `<label for="${this.property}_label_${i}"> ${option[this.optionDisplayProperty]} </label>`
        e += '<br>';

        return e;
      }).join(' ');
    } else {
      if (!this.options.every(option => typeof option === 'string')) throw new Error();

      element = this.options.map((option, i) => {
        let e = `<input type="checkbox"
        ${this.render_name}
        value="${option}"
        id="${this.property}_label_${i}"
        ${this.value && (this.value === option || this.value.find(elem => elem === option)) ? 'checked' : ''}>`;

        e += `<label for="${this.property}_label_${i}"> ${option} </label>`
        e += '<br>';

        return e;
      }).join(' ');
    }

    element += `<input type="hidden" value="" name="${this.property}">`; // Add hidden input to make sure we always return a value
    if (this.label) element = `<legend>${this.label}</legend> ${element}`;

    return element;
  }

  /**
   * @override
   * @returns {Array}
   */
  getData(object) {
    if (this.value === undefined || this.value === null) return [];
    if (!(this.value instanceof Array)) this.value = [this.value];
    this.value = this.value.filter(element => element.length > 0);
    object[this.property] = this.value;
  }

  /**
   * @override
   * @returns {Array}
   */
  getDataRaw() {
    if (this.value === undefined || this.value === null) return [];
    if ((this.value instanceof Array) === false) this.value = [this.value];
    this.value = this.value.filter(element => element.length > 0);

    return this.value;
  }

  isChecked(option) {
    return this.value === option[this.optionValueProperty] || this.value.find(elem => elem === option[this.optionValueProperty]) ? 'checked' : '';
  }
}

exports.CheckboxField = CheckboxField;