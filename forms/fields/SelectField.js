const { Field } = require('./Field');

class SelectField extends Field {
  constructor({ property, required = false, label, value, options = [], optionDisplayProperty, extractData, optionValueProperty } = {}) {
    super({ property, required, label, value, extractData });

    this.options = options;
    this.optionDisplayProperty = optionDisplayProperty;
    this.optionValueProperty = optionValueProperty;
  }

  /**
   * @override
   * @returns {String} HTML
   */
  render() {
    if (this.rendered) return '';
    this.rendered = true;
    if (!(this.options instanceof Array)) throw new Error();

    let options = this.value ? '' : `<option selected disabled>Select one*</option>`;
    if (this.optionDisplayProperty && this.optionValueProperty) {
      options += this.options.map(option => {
        return `<option value="${option[this.optionValueProperty]}" ${this.value !== undefined && this.value !== null && (this.value.toString() === option[this.optionValueProperty].toString()) ? 'selected' : ''}> ${option[this.optionDisplayProperty]} </option>`;
      }).join(' ');
    } else {
      if (!this.options.every(option => typeof option === 'string')) throw new Error();
      options += this.options.map(option => `<option value="${option}" ${option === this.value ? 'selected' : ''}> ${option} </option>`).join(' ');
    }

    let element = `<select ${this.render_name} ${this.render_required}> ${options} </select>`;
    element = this.render_label(element);

    return element;
  }
}

exports.SelectField = SelectField;