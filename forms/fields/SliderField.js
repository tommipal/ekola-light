const { Field } = require('./Field');

class SliderField extends Field {
  constructor({ property, required = false, label, value, placeholder, min, max } = {}) {
    super({ property, required, placeholder, label, value });
    this.min = min;
    this.max = max;
  }

  /**
   * @override
   * @returns {String} HTML
   */
  render() {
    if (this.rendered) return '';
    this.rendered = true;

    const id = `${this.property}-slider`
    let element = `
      <div class="grid-x grid-margin-x">
        <div class="cell small-10">
          <div class="slider" data-slider ${this.value ? `data-initial-start="${this.value}"` : ''} ${this.min !== undefined ? `data-start="${this.min}"` : ''} ${this.max !== undefined ? `data-end="${this.max}"` : ''}>
            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="${id}"></span>
            <span class="slider-fill" data-slider-fill></span>
          </div>
        </div>
        <div class="cell small-2">
          <input name="${this.property}" type="number" id="${id}" ${this.min !== undefined ? `min="${this.min}"` : ''} ${this.max !== undefined ? `max="${this.max}"` : ''}>
        </div>
      </div>`;

    element = this.render_label(element);

    return element;
  }

  /**
   * @override
   */
  getData(object) {
    if (isNaN(this.value)) return;

    this.value = parseInt(this.value);
    object[this.property] = this.value;
  }
  /**
   * @override
   */
  getDataRaw() {
    if (isNaN(this.value)) return;
    this.value = parseInt(this.value);

    return this.value;
  }
}

exports.SliderField = SliderField;