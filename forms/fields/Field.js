class Field {
  constructor({ property, required = false, label, placeholder, value, extractData, populate, validate } = {}) {
    this.property = property;
    this.required = required;
    this.label = label;
    this.placeholder = placeholder;
    this.value = value;

    if (validate && !(validate instanceof Function)) throw new Error();
    this.validate = validate;
    if (extractData && !(extractData instanceof Function)) throw new Error();
    this.extractData = extractData;
    if (populate && !(populate instanceof Function)) throw new Error();
    this.populate = populate;

    this.rendered = false;
  }

  /**
   * Override as needed.
   * @param {Object} object
   */
  getData(object) {
    if (!object || !(object instanceof Object)) throw new Error();

    if (this.sanitizeData) this.sanitizeData();
    if (this.extractData) object[this.property] = this.extractData(this.value);
    else object[this.property] = this.value;
  }

  /**
   * Override as needed.
   * @returns {Any}
   */
  getDataRaw() {
    if (this.sanitizeData) this.sanitizeData();
    if (this.extractData) return this.extractData(this.value);

    return this.value;
  }

  /**
   * @returns {String}
   */
  render() {
    throw new Error('Implement me!');
  }

  render_label(element) {
    if (this.label) element = `<label>${this.label} ${element} </label>`;

    return element;
  }
  get render_name() {
    return `name="${this.property}"`;
  }
  get render_max() {
    return this.max !== undefined ? `min="${this.max}"` : '';
  }
  get render_min() {
    return this.min !== undefined ? `min="${this.min}"` : '';
  }
  get render_placeholder() {
    return this.placeholder ? `placeholder="${this.placeholder}"` : '';
  }
  get render_required() {
    return this.required ? 'required' : '';
  }
  get render_value() {
    return this.value !== undefined ? `value="${this.value}"` : '';
  }
}

exports.Field = Field;