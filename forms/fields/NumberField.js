const { Field } = require('./Field');

class NumberField extends Field {
  constructor({ property, required = false, label, value, placeholder, min, max } = {}) {
    super({ property, required, placeholder, label, value });
  }

  /**
   * @override
   * @returns {String} HTML
   */
  render() {
    if (this.rendered) return '';
    this.rendered = true;

    let element = `<input type="number" ${this.render_name} ${this.render_required} ${this.render_value} ${this.render_min} ${this.render_max}>`

    return element
  }

  /**
   * @override
   */
  getData(object) {
    if (isNaN(this.value)) return;

    this.value = parseInt(this.value);
    object[this.property] = this.value;
  }
  /**
   * @override
   */
  getDataRaw() {
    if (isNaN(this.value)) return;
    this.value = parseInt(this.value);

    return this.value;
  }
}

exports.NumberField = NumberField;