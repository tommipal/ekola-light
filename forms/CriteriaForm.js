const { Form } = require('forms');
const { Utils } = require('common');

class CriteriaForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx, object) {
    super(ctx, object);

    this.fields = [
      new this._fields.TextField({
        label: this.t('common.name'),
        property: 'name',
        type: 'text',
        placeholder: this.t('common.name'),
        required: true
      })
    ];

    this.init();

    this.populate();
  }

  static async build(ctx, object) {
    const form = new CriteriaForm(ctx, object);
    form.populate();

    return form;
  }

  /**
   * Create fields based on the type of criteria.
   */
  init() {
    let fields = [];
    // Scale
    if (!this.object || this.object.type === 'scale') {
      fields = [
        new this._fields.NumberField({
          label: this.t('criteria.scale.min_value'),
          property: 'minValue',
          type: 'number',
          value: 1,
          required: true
        }),
        new this._fields.NumberField({
          label: this.t('criteria.scale.max_value'),
          property: 'maxValue',
          type: 'number',
          value: 5,
          required: true
        })
      ];
      this.validation.push(function () {
        const min = this.findField('minValue').value;
        const max = this.findField('maxValue').value;

        const isValid = (isNaN(min) || isNaN(max)) ? false : (parseInt(min) < parseInt(max));
        if (!isValid) this.showError('criteria.scale.error');

        return isValid;
      }.bind(this));
    }
    else if (this.object.type === 'select' || this.object.type === 'checkbox') { // Select + Checkbox
      fields = [
        new this._fields.TextareaField({
          label: this.t('criteria.select.options'),
          property: 'options',
          placeholder: this.t('criteria.select.option_placeholder'),
          required: true,
          rows: 6,

          // Converts array to a string that can be used with an input
          populate: function (value) {
            if (value && value instanceof Array) return value.join('; &#013;');

            return value;
          }.bind(this),

          // Converts string from input to an array
          extractData: function (str) {
            if (typeof str === 'string') {
              let arr = str.split(';');
              for (let i = 0; i < arr.length; i++) arr[i] = Utils.trimString(arr[i]);
              arr = arr.filter(element => element.length > 0);

              return arr;
            }
            return str;
          }.bind(this)
        }),
      ];
    }

    if (fields.length > 0) this.fields = this.fields.concat(fields);
  }
}

module.exports = CriteriaForm;