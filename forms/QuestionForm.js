const { Form } = require('forms');

class QuestionForm extends Form {
  /**
   * @param {Object} ctx Koa context
   */
  constructor(ctx, object) {
    super(ctx, object);

    this.fields = [
      new this._fields.TextField({
        label: this.t('common.name'),
        property: 'name',
        type: 'text',
        placeholder: this.t('common.name'),
        required: true
      }),
      new this._fields.TextareaField({
        label: this.t('common.description'),
        property: 'description',
        type: 'textarea',
        rows: 5,
        placeholder: this.t('common.description'),
        required: false,
      })];
  }

  static async build(ctx, object) {
    const form = new QuestionForm(ctx, object);
    form.populate();

    return form;
  }
}

module.exports = QuestionForm;