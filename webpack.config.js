const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const { config } = require('common');

const plugins_dev = [];
const plugins_prod = [];

const webpackConfig = {
  mode: config.env,
  entry: {
    'js/criteria/edit':       [path.join(config.appRoot, 'client', 'scripts', 'criteria', 'edit')],
    'js/results':             [path.join(config.appRoot, 'client', 'scripts', 'survey', 'results')],
    'js/view':                [path.join(config.appRoot, 'client', 'scripts', 'survey', 'view')],
    'js/survey/edit':         [path.join(config.appRoot, 'client', 'scripts', 'survey', 'edit')],
    'js/user/list':           [path.join(config.appRoot, 'client', 'scripts', 'user', 'list')],
    'js/user/listUnverified': [path.join(config.appRoot, 'client', 'scripts', 'user', 'listUnverified')],
    'js/user/profile':        [path.join(config.appRoot, 'client', 'scripts', 'user', 'profile')],
    'js/question/edit':       [path.join(config.appRoot, 'client', 'scripts', 'question', 'edit')],
  },
  output: {
    publicPath: '/',
    filename: '[name].js',
    path: path.join(config.appRoot, 'dist'),
  },
  context: path.join(config.appRoot, 'client'),
  plugins: config.env === 'production' ? plugins_prod : plugins_dev,
  node: { console: false },
  resolve: {
    extensions: ['.js', '.css'],
    modules: [
      path.resolve(config.appRoot, 'client', 'scripts'),
      path.resolve(config.appRoot, 'node_modules'),
      path.resolve(config.appRoot, 'extensions', 'translator'),
    ]
  },
  optimization: {
    minimizer: [new UglifyJsPlugin({
      parallel: true,
    })],
  },
};

// webpackConfig.devtool = 'cheap-module-source-map';

module.exports = webpackConfig;