const crypto = require('crypto');
const fs = require('fs');
const moment = require('moment');

const config = require('./config');

const algorithm = 'aes-256-cbc';
const key = config.encryption_key;

/**
 * Courtesy of megakoresh@gmail.com
 */
class Utils {
  /**
   * @param {String} text Text to be encrypted
   * @returns {Promise<{ encrypted: string, iv: string }> } Encrypted value and the randomly generated  initialization vector used in the encrypting process
   */
  static async encrypt(text) {
    if (!text) throw new Error();
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(algorithm, key, iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');

    return { encrypted, iv: iv.toString('hex') };
  }

  /**
   * @param {String} text Text to decrypt
   * @param {String} iv Initialization vector used to encrypt the value
   */
  static async decrypt(text, iv) {
    if (!text || !iv) throw new Error();
    iv = Buffer.from(iv, 'hex');
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    let decrypted = decipher.update(text, 'hex', 'utf8');
    decrypted += decipher.final('utf8');

    return decrypted;
  }

  /**
   * Return (awaitable) promise to delays execution
   * @param {Number} milliseconds
   * @returns {Promise<null>}
   */
  static delay(milliseconds) {
    return new Promise(function (cb) {
      setTimeout(cb, milliseconds);
    });
  }

  /**
  * @param {Number} trueProbability value between 0 and 1. Defaults to 0.5 (=50%)
  */
  static getRandomBoolean(trueProbability = 0.5) {
    return Math.random() < trueProbability;
  }

  /**
   * Returns a past/future date matching the given boundaries.
   * @param {Number} minDays
   * @param {Number} maxDays
   */
  static getRandomDate(minDays, maxDays) {
    if (Math.random() > 0.5) return Utils.getRandomFutureDate(minDays, maxDays);
    return Utils.getRandomPastDate(minDays, maxDays);
  }

  /**
   * @param {Number} minDays
   * @param {Number} maxDays
   */
  static getRandomFutureDate(minDays, maxDays) {
    const minMinutes = 1440 * minDays;
    const maxMinutes = 1440 * maxDays;

    return moment().add(Utils.getRandomInteger(minMinutes, maxMinutes), 'minutes');
  }

  /**
   * @param {Number} minDays
   * @param {Number} maxDays
   */
  static getRandomPastDate(minDays, maxDays) {
    const minMinutes = 1440 * minDays;
    const maxMinutes = 1440 * maxDays;

    return moment().subtract(Utils.getRandomInteger(minMinutes, maxMinutes), 'minutes');
  }

  /**
   * @param {Number} min
   * @param {Number} max
   */
  static getRandomInteger(min = 1, max = 10) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  /**
   * @param {Number} length
   * @returns {String} Randomly generated string
   */
  static getRandomString(length) {
    if (!length) length = 10;
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  /**
   * Returns the last element of an array
   * @param {Array} array
   */
  static last(array = []) {
    if (!array instanceof Array) throw new Error('Utils.last: Expected an array');
    if (array.length < 1) return null;

    return array[array.length - 1];
  }

  static sanitizeString(str) {
    if (!str) return '';
    str = str.replace(/[^a-z0-9áéíóúñüäöå ?;\.,_-\s]/gim, "");

    return str.trim();
  }

  static trimString(str) {
    if(!str) return '';
    str = str.replace(/[^a-z0-9áéíóúñüäöå ?;\.,_-]/gim, "");

    return str.trim();
  }

  /**
   * @param {Array} from
   * @param {Boolean} remove
   * @returns {any}
   */
  static selectRandom(from, remove = false) {
    if (!Array.isArray(from)) throw new Error("from must be an array of choices! Was " + from);
    if (from.length === 0) throw new Error("Can't select from an empty array");
    let rand = Utils.getRandomInteger(0, from.length - 1);
    let value = from[rand];
    if (remove === true) {
      from.splice(rand, 1);
    }

    return value;
  }

  /**
  * Returns a generator that will iterate an object's enumerable properties, compatible with for..of loops
  * @param {*} object whose enumerable properties will be iterated
  * @returns {Generator} a generator object that conforms to the iterator protocol
  */
  static * iterateObject(object) {
    for (let key in object) {
      if (object.hasOwnProperty(key)) {
        yield [key, object[key]];
      }
    }
  }

  /**
   *
   * @param {String} folderPath
   * @param {String} namespace
   */
  static requireNamespace(folderPath, namespace) {
    const modules = {};
    const path = require('path');
    const directory = path.join(config.appRoot, folderPath);

    const files = require('fs').readdirSync(directory)

    let ignoreRegex = /(index)(\.js)/;

    const dirStats = fs.statSync(folderPath); //throws if the directory doesnt exist or permission is denied

    if (!dirStats.isDirectory()) throw new Error(`${folderPath} is not a directory!`);

    function* walk(directory) {
      const files = fs.readdirSync(directory);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        if (file.match(ignoreRegex)) continue;
        const stat = fs.statSync(path.join(directory, file));
        if (stat.isDirectory()) yield* walk(path.join(directory, file));
        else yield path.join(directory, file);
      }
    }

    const iterator = walk(directory);
    let result = iterator.next();
    while (!result.done) {
      if (result.value.endsWith('.ts')) {
        result = iterator.next();
        continue;
      }
      let m = require(result.value);
      if (m[namespace]) {
        //store potentially incomplete reference containing the namespace here
        modules[path.basename(result.value, '.js')] = m;
      }
      result = iterator.next();
    }

    const moduleNames = Object.keys(modules);
    for (let moduleName of moduleNames) {
      modules[moduleName] = modules[moduleName][namespace];
    }

    return modules;
  }
}

module.exports = Utils;