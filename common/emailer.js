const nodemailer = require('nodemailer');
const { config } = require('common');

class Emailer {
  /**
   * Send email ONLY if NODE_ENV is set to 'production'.
	 * @param {Array<String>} to Array of email addresses
	 * @param {String} subject subject of email
	 * @param {String} text text content
	 * @param {String} html html content
   * @returns {void}
	 */
  static sendEmail({ to = [], subject = 'No subject', text = '', html = '' } = {}) {
    if ((to instanceof Array) === false) throw new Error(`Expected an array of email addresses. Found ${to}`);

    const params = {
      from: config.emailer.from,
      to: to.join(', '),
      subject: subject,
      text: text,
      html: html
    }
    if (process.env.NODE_ENV !== 'production') return console.info(`Emailer:\n${JSON.stringify(params)}`);

    /**
     * TODO create ejs templates for emails
     */
    let transporterParams;
    if (config.emailer.transport == 'smtp') {
      transporterParams = {
        sendmail: true,
        newline: config.emailer.sendmail.newline,
        path: config.emailer.sendmail.path
      };
    } else if (config.emailer.transport == 'sendmail') {
      transporterParams = {
        host: config.emailer.smtp.host,
        port: config.emailer.smtp.port,
        secure: config.emailer.smtp.secure,
        auth: {
          user: config.emailer.smtp.user,
          pass: config.emailer.smtp.password,
        }
      }
    } else throw new Error(`Emailer: Expected transport to be 'smtp' or 'sendmail'. Found ${config.emailer.transport}`);

    const transporter = nodemailer.createTransport(transporterParams);

    transporter.sendMail(params, (err, info) => {
      if (err) console.error(err);
      if (info) console.info(info);
    });
  }
}

module.exports = Emailer;