const path = require('path');

module.exports = {
  admin_email: process.env['EKOLA_ADMIN_EMAIL'],
  admin_password: process.env['EKOLA_ADMIN_PASSWORD'],
  encryption_key: 'jlJUajlkadsjlkjlk946lkdsjlkdsjlk',
  csrf: {
    "invalidSessionSecretMessage": "Invalid session secret",
    "invalidSessionSecretStatusCode": 403,
    "invalidTokenMessage": "Invalid CSRF token",
    "invalidTokenStatusCode": 403,
    "excludedMethods": ["GET", "HEAD", "OPTIONS"],
    "disableQuery": false
  },
  keys: {
    session: process.env['EKOLA_SESSION_SECRET'] ? process.env['EKOLA_SESSION_SECRET'].split(',') : ['session-secret'],
  },
  appRoot: __dirname.split(path.sep).slice(0, -1).join(path.sep),
  env: process.env['NODE_ENV'],
  defaultLocale: 'en',

  //Database
  databaseHost:     process.env['EKOLA_DATABASE_HOST'],
  database:         process.env['EKOLA_DATABASE'],
  databaseUser:     process.env['EKOLA_DATABASE_USER'],
  databasePassword: process.env['EKOLA_DATABASE_PASSWORD'],
  databaseConfig: {
    useNewUrlParser: true,
    useCreateIndex: true,
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    keepAlive: true,
    keepAliveInitialDelay: 300000,
  },

  //Session
  session: {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    /** (number || 'session') maxAge in ms (default is 1 days) */
    /** 'session' will result in a cookie that expires when session/browser is closed */
    /** Warning: If a session cookie is stolen, this cookie will never expire */
    /** Warning#2: Stealing cookies is prohibited by the Official EU Bakery protection policy */
    maxAge: 86400000,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. default is false **/
  },

  emailer: {
    transport: process.env['EKOLA_EMAILER_TRANSPORT'] || 'sendmail', // (sendmail / smtp)
    from: process.env['EKOLA_EMAILER_FROM'] || '"Ekola" <ekola.no-reply@metropolia.fi>',
    sendmail: {
      path: process.env['EKOLA_SENDMAIL_PATH'] || '/usr/sbin/sendmail',
      newline: process.env['EKOLA_SENDMAIL_NEWLINE'] || 'unix',
    },
    smtp: {
      host: process.env['EKOLA_SMTP_HOST'],
      port: process.env['EKOLA_SMTP_PORT'] || 465,
      user: process.env['EKOLA_SMTP_USER'],
      password: process.env['EKOLA_SMTP_PASSWORD'],
      secure: true,
    }
  },
}