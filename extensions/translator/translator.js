const translations = require('./translations');

class Translator {
  /**
   * @param {string} locale
   */
  constructor(locale) {
    this.locale = locale;
  }

  /**
   * @returns {Array<string>}
   */
  static supportedLocales() {
    return ['en', 'fi'];
  }

  /**
   * @param {string} key
   * @param {Object} params
   * @returns {string}
   */
  translate(key, params) {
    return Translator.translate(key, this.locale, params);
  }

  /**
   * @param {String} key
   * @param {String} locale
   * @param {Object} params
   */
  static translate(key, locale, params = {}) {
    if (!key instanceof String || key.length < 1) throw new Error('Expected the key to be a String!');
    if (!this.supportedLocales().some(supported => supported === locale)) throw new Error(`Locale '${locale}' isn't supported!`);

    const keys = key.split('.');

    let translation = translations;
    for (let i = 0; i < keys.length; i++) {
      if (!translation[keys[i]]) return `Missing translation for key '${key}'`;
      translation = translation[keys[i]];
    }
    if (!translation[locale]) return `Missing translation for key '${key}' for locale ${locale}`;

    let str = translation[locale];

    for (let [name, value] of this.iterateObject(params)) str = str.replace(`:${name}`, value);

    return str;
  }

  /**
   * @param {Object} object whose enumerable properties will be iterated
   * @returns {Generator} a generator object that conforms to the iterator protocol
   */
  static * iterateObject(object) {
    for (let key in object) {
      if (object.hasOwnProperty(key)) {
        yield [key, object[key]];
      }
    }
  }
}

(function (exports) {
  exports.Translator = Translator;
})(typeof exports === 'undefined' ? this['translatorModule'] = {} : exports);