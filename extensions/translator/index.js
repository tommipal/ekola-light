const Koa = require('koa');
const Translator = require('./translator').Translator;

exports.load = function (app) {
  if (!(app instanceof Koa)) throw new Error();

  app.context.getLocale = function () {
    if (!this.session.locale || !Translator.supportedLocales().some(locale => locale === this.session.locale)) this.session.locale = 'en';

    return this.session.locale;
  }

  app.context.t = function (key, params = {}) {
    if (!this.session.locale || !Translator.supportedLocales().some(locale => locale === this.session.locale)) this.session.locale = 'en';

    return Translator.translate(key, this.session.locale, params);
  }

  app.context.supportedLocales = function () {
    return Translator.supportedLocales();
  }
}