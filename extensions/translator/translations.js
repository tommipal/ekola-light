module.exports = {
  // --- Criteria ---
  criteria: {
    confirmDelete: { en: 'Are you sure you want to delete the selected criteria?', fi: 'Haluatko varmasti poistaa valitun alikysymyksen?' },
    edit: {
      title: { en: 'Edit criteria', fi: 'Muokkaa alikysymystä' },
      backToSurvey: { en: 'Back to survey', fi: 'Takaisin kyselyyn' },
      saved: { en: 'Criteria saved!', fi: 'Alikysymys tallennettu!' },
      deleted: { en: 'Criteria deleted!', fi: 'Alikysymys poistettu!' },
    },
    select: {
      options: { en: 'Options', fi: 'Vastausvaihtoehdot' },
      option_placeholder: { en: 'Options separated by semicolon:\nA;\nB;\nC;', fi: 'Vastausvaihtoehdot puolipisteellä eroteltuna:\nA;\nB;\nC;' },
    },
    type: { en: 'Type', fi: 'Tyyppi' },
  },
  // --- Errors ---
  error: {
    emailReserved: { en: 'Selected email was already reserved', fi: 'Sähköposti on jo käytössä' },
    userWasNotFound: { en: `Couldn't find the requested user`, fi: 'Valittua käyttäjää ei löytynyt' },
    forbidden: { en: 'Forbidden', fi: 'Pääsy evätty' },
    criteria: {
      notFound: { en: 'Could not find the requested criteria', fi: 'Pyydettyä alikysymystä ei löytynyt' },
    },
    invalidCredentials: { en: 'Invalid email or password', fi: 'Kirjautuminen epäonnistui: Väärä salasana tai sähköposti' },
    linkNotValid: { en: 'The request link was not valid', fi: 'Valittu linkki ei ollut validi' },
    linkNotFound: { en: 'Could not find the requested link', fi: 'Valittua linkkiä ei löytynyt' },
    question: {
      notFound: { en: 'Could not find the requested question', fi: 'Pyydettyä kysymystä ei löytynyt' },
    },
    survey: {
      notFound: { en: 'Could not find the requested survey', fi: 'Pyydettyä kyselyä ei löytynyt' },
      notPublic: { en: 'Requested survey was not public', fi: 'Pyydetty kysely ei ollut julkinen' },
    },
    userNotVerified: { en: 'Account has yet to be verified!', fi: 'Käyttäjätunnuksia ei ole vielä varmennettu' },
    view: {
      title: { en: 'Oops! Something went wrong..', fi: 'Hupsis! Jotain meni pieleen..' },
      description: { en: 'If the problem occures again, please contact support.', fi: 'Jos ongelma toistuu, ole hyvä ja ota yhteyttä ylläpitoon.' },
    }
  },
  // --- Evaluation ---
  evaluation: {
    archived: {
      view: { en: 'View archived evaluations', fi: 'Tarkastele arkistoituja arviointiryhmiä' },
      title: { en: 'Archived evaluations', fi: 'Arkistoidut arviointiryhmät' },
    },
    create: { en: 'Create new evaluation', fi: 'Luo uusi arviointiryhmä' },
    confirmDelete: { en: 'Are you sure you want to delete the selected evaluation?', fi: 'Haluatko varmasti poistaa valitun arviointiryhmän?' },
    add: {
      groups: { en: 'Add groups to evaluation', fi: 'Lisää ryhmiä' },
    },
    delete: {
      en: 'Delete evaluation', fi: 'Poista arviointiryhmä',
      confirm: { en: 'Are you sure you want to delete the selected evaluation?', fi: 'Haluatko varmasti poistaa valitun arviointiryhmän?' }
    },
    edit: {
      title: { en: 'Edit evaluation', fi: 'Muokkaa arviointiryhmää' },
      saved: { en: 'Evaluation saved!', fi: 'Arviointiryhmä tallennettu!' },
      deleted: { en: 'Evaluation deleted!', fi: 'Arviointiryhmä poistettu!' },
    },
    groups: { en: 'Groups', fi: 'Ryhmät' },
    numberOfGroups: { en: 'Groups', fi: 'Ryhmiä' },
  },
  // --- Group ---
  group: {
    archived: {
      view: { en: 'View archived groups', fi: 'Tarkastele arkistoituja ryhmiä' },
      title: { en: 'Archived groups', fi: 'Arkistoidut ryhmät' },
    },
    create: { en: 'Create new group', fi: 'Luo uusi ryhmä' },
    confirmDelete: { en: 'Are you sure you want to delete the selected group?', fi: 'Haluatko varmasti poistaa valitun ryhmän?' },
    delete: {
      en: 'Delete group', fi: 'Poista ryhmä',
      confirm: { en: 'Are you sure you want to delete the selected group?', fi: 'Haluatko varmasti poistaa valitun ryhmän?' }
    },
    edit: {
      title: { en: 'Edit group', fi: 'Muokkaa ryhmää' },
      saved: { en: 'Group saved!', fi: 'Ryhmä tallennettu!' },
      deleted: { en: 'Group deleted!', fi: 'Ryhmä poistettu!' },
    },
    members: {
      create: { en: 'Create new member', fi: 'Lisää uusi jäsen' },
      count: { en: 'Members', fi: 'Jäseniä' },
      manage: { en: 'Manage group members', fi: 'Hallinnoi jäseniä' },
      title: { en: 'Members', fi: 'Jäsenet' },
    },
    member: {
      edit: { en: 'Edit member information', fi: 'Muokkaa jäsenen tietoja' },
    },
    numberOfMembers: { en: 'Members', fi: 'Jäseniä' },
    title: { en: 'Group', fi: 'Ryhmä' },
  },
  // --- User ---
  user: {
    createdAt: { en: 'Account created', fi: 'Käyttäjä luotu' },
    profile: {
      title: { en: 'Profile', fi: 'Profiili' },
      header: { en: 'Your Profile', fi: 'Oma Profiili' },
      surveys: { en: 'Your Surveys', fi: 'Omat Kyselyt' },
      evaluations: { en: 'Your Evaluations', fi: 'Omat Arviointiryhmät' },
      groups: { en: 'Your Groups', fi: 'Omat Ryhmät' },
    },
    confirmPassword: { en: 'Confirm Password', fi: 'Varmista salasana' },
    verify: { en: 'Verify', fi: 'Vahvista' },
    verifyNewUser: { en: 'Are you sure you want to verify the selected user?', fi: 'Haluatko varmasti vahvistaa valitun käyttäjän?' },
    list: {
      isAdmin: { en: 'Admin', fi: 'Ylläpitäjä' },
      title: { en: 'Old users', fi: 'Nykyiset käyttäjät' },
      promote: { en: 'Grant admin privileges', fi: 'Lisää ylläpitäjän oikeudet' },
      promoteUser: { en: 'Are you sure you want to grant admin privileges to the selected user?', fi: 'Haluatko varmasti myöntää ylläpitäjän oikeudet valitulle käyttäjälle?' },
      demote: { en: 'Revoke admin privileges', fi: 'Poista ylläpitäjän oikeudet' },
      demoteUser: { en: 'Are you sure you want to revoke admin privileges from the selected user?', fi: 'Haluatko varmasti poistaa ylläpitäjän oikeudet valitulta käyttäjältä?' },
    },
    listUnverified: { en: 'New users', fi: 'Uudet käyttäjät' },
    login: { en: 'Login', fi: 'Kirjaudu sisään' },
    logout: { en: 'Logout', fi: 'Kirjaudu ulos' },
    locale: { en: 'locale', fi: 'Kieli' },
    register: { en: 'Register', fi: 'Rekisteröidy' },
    reject: { en: 'Reject', fi: 'Hylkää' },
    rejectNewUser: { en: 'Are you sure you want to reject the selected user?', fi: 'Haluatko varmasti hylätä valitun käyttäjän?' },
    password: { en: 'Password', fi: 'Salasana' },
    successfullyRegistered:
    {
      en: 'Registered successfully! You will be able to login once your application has been verified.',
      fi: 'Rekisteröinti onnistui! Pystytte kirjautumaan järjestelmään ylläpidon vahvistettua hakemuksenne.'
    },
  },
  // --- Survey ---
  survey: {
    add: {
      question: { en: 'Add new question', fi: 'Lisää uusi kysymys' }
    },
    archive: { en: 'Archive', fi: 'Arkistoi' },
    archived: {
      view: { en: 'View archived surveys', fi: 'Tarkastele arkistoituja kyselyitä' },
      title: { en: 'Archived surveys', fi: 'Arkistoidut kyselyt' },
    },
    confirmDelete: { en: 'Are you sure you want to delete the selected survey?', fi: 'Haluatko varmasti poistaa valitun kyselyn?' },
    copy: { en: 'Copy', fi: 'Kopio' },
    create: { en: 'Create new survey', fi: 'Luo uusi kysely' },
    closes: { en: 'Closes', fi: 'Sulkeutuu' },
    delete: {
      en: 'Delete survey', fi: 'Poista kysely',
      confirm: { en: 'Are you sure you want to delete the selected survey?', fi: 'Haluatko varmasti poistaa valitun kyselyn?' }
    },
    dates: {
      closingDate: { en: 'Closing date', fi: 'Sulkeutumispäivämäärä' },
      openingDate: { en: 'Opening date', fi: 'Avautumispäivämäärä' },
    },
    edit: {
      evaluation: { en: 'Evaluation', fi: 'Arviointiryhmä' },
      linkCopied: { en: 'Copied link: :link', fi: 'Kopioitiin linkki: :link' },
      manageQuestions: { en: 'Manage questions', fi: 'Hallinnoi kysymyksiä' },
      notify: { en: 'Notify answerers', fi: 'Lähetä vastaajille kutsu' },
      notifyWarning: {
        en: 'Are you sure you want to send an invite to all the members of the evaluation?',
        fi: 'Haluatko varmasti lähettää kutsun kaikille arviointiryhmän jäsenille?'
      },
      notificationSent: { en: 'Invites were sent successfully!', fi: 'Kutsut lähetettiin onnistuneesti!' },
      survey: { en: 'Survey', fi: 'Kysely' },
      questions: { en: 'Questions', fi: 'Kysymykset' },
      results: { en: 'Results', fi: 'Tulokset' },
      saveAsTemplate: { en: 'Save as template*', fi: 'Tallenna pohjaksi*' },
      sharing: { en: 'Sharing', fi: 'Jakaminen' },
      title: { en: 'Edit survey', fi: 'Muokkaa kyselyä' },
      saved: { en: 'Survey saved!', fi: 'Kysely tallennettu!' },
      deleted: { en: 'Survey deleted!', fi: 'Kysely poistettu!' },
    },
    evaluation: { en: 'Evaluation', fi: 'Arviointiryhmä' },
    notification: {
      link: { en: 'Answer', fi: 'Vastaa' },
      invite: {
        en: 'You have been invided to answer to survey :survey. The survey opens at :opensAt and closes :closesAt. You can access the survey with the link below.',
        fi: 'Sinut on kutsuttu vastaamaan kyselyyn :survey. Kysely aukeaa vastauksille :opensAt ja sulkeutuu :closesAt. Pääset vastaamaan allaolevasta linkistä.'
      },
    },
    noActiveEvaluation: { en: `Survey doesn't have an active evaluation`, fi: 'Kyselyä ei ole liitetty mihinkään arviointiryhmään' },
    numberOfQuestions: { en: 'Questions', fi: 'Kysymyksiä' },
    is: {
      public: { en: 'Public', fi: 'Julkinen' },
      closed: { en: 'Survey is currently closed', fi: 'Kysely on tällä hetkellä suljettu' },
    },
    opens: { en: 'Opens', fi: 'Avautuu' },
    open: { en: 'Open', fi: 'Avoin' },
    public: {
      view: { en: 'View public surveys', fi: 'Tarkastele julkisia kyselyitä' },
      title: { en: 'Public surveys', fi: 'Julkiset kyselyt' },
    },
    results: {
      numberOfAnswers: { en: 'No. Answers', fi: 'Vastauksia' },
      download: { en: 'Download results', fi: 'Lataa tulokset' },
      title: { en: 'Results', fi: 'Tulokset' },
      view: { en: 'View results', fi: 'Tarkastele vastauksia' },
      totalNumberOfAnswers: { en: 'Total number of answers', fi: 'Vastauksia yhteensä' },
      averageValue: { en: 'Average value', fi: 'Keskimääräinen arvo' },
    },

  },
  // --- Question ---
  question: {
    add: {
      criteria: {
        scale: { en: 'Scale', fi: 'Skaala' },
        select: { en: 'Select', fi: 'Valitse yksi' },
        text: { en: 'Text', fi: 'Teksti' },
        checkbox: { en: 'Checkbox', fi: 'Monivalinta' },
      }
    },
    confirmDelete: { en: 'Are you sure you want to delete the selected question?', fi: 'Haluatko varmasti poistaa valitun kysymyksen?' },
    criteria: {
      manage: { en: 'Manage criteria', fi: 'Hallitse alikysymyksiä' },
      confirmDelete: { en: 'Are you sure you want to delete the selected criteria?', fi: 'Haluatko varmasti poistaa valitun alikysymyksen?' },
      count: { en: 'Number of criteria', fi: 'Alikysymyksiä' },
      type: { en: 'Type', fi: 'Tyyppi' },
    },
    description: { en: 'Description of the question', fi: 'Kysymyksen kuvaus' },
    edit: {
      title: { en: 'Edit question', fi: 'Muokkaa kysymystä' },
      saved: { en: 'Question saved!', fi: 'Kysymys tallennettu!' },
      deleted: { en: 'Question deleted!', fi: 'Kysymys poistettu!' },
    }
  },
  // --- Common ----
  common: {
    activate: { en: 'Activate', fi: 'Aktivoi' },
    add: { en: 'Add', fi: 'Lisää' },
    addSelected: { en: 'Add selected', fi: 'Lisää valitut' },
    archive: { en: 'Archive', fi: 'Arkistoi' },
    back: { en: 'Back', fi: 'Takaisin' },
    cancel: { en: 'Cancel', fi: 'Peruuta' },
    collapse: { en: 'Collapse all', fi: 'Pienennä kaikki' },
    confirm: { en: 'Confirm', fi: 'Vahvista' },
    cookies:
    {
      en: 'This website uses cookies to save your settings. By pressing log in, you consent to the usage of cookies',
      fi: 'Tämä sivusto käyttää evästeitä asetustesi tallentamiseen. Painamalla \"kirjaudu sisään\" hyväksyt evästeiden käytön.'
    },
    copy: { en: 'Copy', fi: 'Kopioi' },
    delete: { en: 'Delete', fi: 'Poista' },
    description: { en: 'Description', fi: 'Kuvaus' },
    edit: { en: 'Edit', fi: 'Muokkaa' },
    email: { en: 'Email', fi: 'Sähköposti' },
    error: { en: 'Error!', fi: 'Virhe!' },
    important: { en: 'Note!', fi: 'Huom!' },
    locale: { en: 'Locale', fi: 'Kieli' },
    name: { en: 'Name', fi: 'Nimi' },
    no: { en: 'No', fi: 'Ei' },
    remove: { en: 'Remove', fi: 'Poista' },
    saved: { en: 'Saved!', fi: 'Tallennettu!' },
    submit: { en: 'Submit', fi: 'Tallenna' },
    update: { en: 'Update', fi: 'Päivitä' },
    view: { en: 'View', fi: 'Tarkastele' },
    yes: { en: 'Yes', fi: 'Kyllä' },
  },
  //TYPES
  scale: { en: 'Scale', fi: 'Skaala' },
  select: { en: 'Select', fi: 'Valitse yksi' },
  checkbox: { en: 'Checkbox', fi: 'Monivalinta' },
  text: { en: 'Text', fi: 'Teksti' },
}