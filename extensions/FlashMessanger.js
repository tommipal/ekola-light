const Koa = require('koa');

exports.load = function (app) {
  if (!(app instanceof Koa)) throw new Error();

  app.context.getFlashMessanger = function () {
    if (!this.session.flashMessages) this.session.flashMessages = [];

    const createFlashMessage = function ({ message, header, type = 'primary' } = {}) {
      if (message || header) this.session.flashMessages.push({ message, header, type });
    }.bind(this);

    return {
      showMessage: function (message, header = 'common.important') {
        createFlashMessage({ message, header, type: 'success' });
      }.bind(this),

      showAlert: function (message, header = 'common.error') {
        createFlashMessage({ message, header, type: 'alert' });
      }.bind(this),

      showWarning: function (message, header = 'common.warning') {
        createFlashMessage({ message, header, type: 'warning' });
      }.bind(this),

      showCustomMessage: function (params) {
        createFlashMessage(params);
      }.bind(this),

      render: function () {
        let output = '';

        while (this.session.flashMessages.length > 0) {
          const flash = this.session.flashMessages.pop();
          output += `<div class="callout ${flash.type}" data-closable>
          ${flash.header ? '<h5>' + this.t(flash.header) + '</h5>' : ''}
          <p>${this.t(flash.message)}</p>
          <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
          </button>
          </div>`;
        }

        return output;
      }.bind(this)
    }
  }
}