const models = require('models');

before(async function () {
  this.timeout(30000);

  await models.load();
});

describe('user_add_admin', async function () {
  it('', async function () {
    const arg = process.argv.find(arg => arg.includes('email:'));
    if (!arg) {
      console.info(`Error: Expected arg --email:*insert email here*`);
      return;
    }

    const email = arg.split('email:')[1];
    const user = await User.findOne({ email: email });
    if (!user) return console.info(`Error: Couldn't find user with email '${email}'`);

    if (user.admin) return console.info(`Info: User '${email}' was already an admin`);
    if (!user.verified) user.verified = true;

    user.admin = true;
    await user.save();
    console.info(`Info: Successfully added admin status for user '${email}'`);
  });
});