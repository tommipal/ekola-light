const models = require('models');
const { config } = require('common');

before(async function () {
  this.timeout(30000);
  if (config.env !== 'development') throw new Error('Test scripts should only be executed in development environment!');

  await models.load();

  console.info('This script will delete all the documents currently in the database.');
});

describe('Purge', async function () {
  it('Remove all documents', async function () {
    const models = [User, Survey, Group, Evaluation];
    for (const model of models) await model.deleteMany({});

    console.info('Finished emptying the database!');
  });
});