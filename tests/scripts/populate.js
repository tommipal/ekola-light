const casual = require('casual');

const models = require('models');
const { config, Utils } = require('common');

// --- Variables ---
//General
const logErrors = true;

// Users
let users = [];
const user_qty = 5;
const unverified_users_qty = 10;
const pwd = 'test';
const user_admin_chace = 0.7;

//Groups
const groups = [];
const group_qty = 40;
const group_members_qty_min = 3;
const group_members_qty_max = 25;
const group_archived_chance = 0.1;

//Evaluations
const evaluations = [];
const evaluation_qty = 15;
const evaluation_groups_min_qty = 1;
const evaluation_groups_max_qty = 6;
const evaluation_archived_chance = 0.1;

//Surveys
const surveys = [];
const survey_qty = 30;
const survey_questions_qty_min = 3;
const survey_questions_qty_max = 28;
const survey_archived_chance = 0.1;
const survey_public_chance = 0.3;
//Criteria
const criteria_qty_min = 1;
const criteria_qty_max = 5;
const criteria_titles = ['Importance', 'Urgency', 'Current state', 'Past state', 'Future state', 'Tärkeys', 'Kiireellisyys', 'Nykyinen tilanne'];

//Sessions (per survey)
sessions_qty_min = 0;
sessions_qty_max = 100;
sessions_chance_to_answer = 0.85;

before(async function () {
  this.timeout(30000);
  if (config.env !== 'development') throw new Error('Test scripts should only be executed in development environment!');

  await models.load();

  console.info('This script will populate the database with test documents. One should only run this script in DEVELOPMENT environment!');
});

describe('Populate', async function () {
  describe(`Users (qty: ${user_qty})`, async function () {
    it(`Adding users`, async function () {
      for (let i = 0; i < user_qty; i++) {
        const password = await User.hashpwd(pwd);
        const verified = true;
        const admin = Utils.getRandomBoolean(user_admin_chace);
        const user = new User({
          email: `user${i}@ekola.fi`,
          password: password.encrypted,
          iv: password.iv,
          verified: verified,
          admin: admin,
        });
        try {
          await user.save();
          users.push(user);
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });

    it(`Adding unverified users`, async function() {
      for (let i = 0; i < unverified_users_qty; i++) {
        const password = await User.hashpwd(pwd);
        const verified = false;
        const admin = false;
        const user = new User({
          email: `unverifieduser${i}@ekola.fi`,
          password: password.encrypted,
          iv: password.iv,
          verified: verified,
          admin: admin,
        });
        try {
          await user.save();
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
  });

  describe(`Groups (qty: ${group_qty})`, async function () {
    it(`Adding groups`, async function () {
      //Initialize new group
      for (let i = 0; i < group_qty; i++) {
        const group = new Group({
          name: `Group ${i}`,
          description: casual.sentence,
          owner: Utils.selectRandom(users)._id,
          archived: Utils.getRandomBoolean(group_archived_chance),
        });

        //Generate members
        for (let ii = 0; ii < Utils.getRandomInteger(group_members_qty_min, group_members_qty_max); ii++) {
          group.members.push({
            email: casual.email,
            name: casual.name,
            description: Utils.getRandomBoolean() ? casual.short_description : ''
          });
        }

        //Save document
        try {
          await group.save();
          groups.push(group);
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
  });

  describe(`Evaluations (qty: ${evaluation_qty})`, async function () {
    it(`Adding evaluations`, async function () {
      //Generate evaluation
      for (let i = 0; i < evaluation_qty; i++) {
        const user = Utils.selectRandom(users);
        const evaluation = new Evaluation({
          name: `Evaluation ${i}`,
          description: casual.sentence,
          owner: user._id,
          archived: Utils.getRandomBoolean(evaluation_archived_chance),
        });

        //Push groups to evaluation
        const possibleGroups = groups.filter(group => group.owner.equals(user._id));

        for (let ii = 0; ii < Utils.getRandomInteger(evaluation_groups_min_qty, evaluation_groups_max_qty); ii++) {
          if (possibleGroups.length > 0) evaluation.groups.push(Utils.selectRandom(possibleGroups, true));
        }

        //Save document
        try {
          await evaluation.save();
          evaluations.push(evaluation);
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
  });

  describe(`Surveys (qty: ${survey_qty})`, async function () {
    it(`Adding surveys`, async function () {
      for (let i = 0; i < survey_qty; i++) {
        const user = Utils.selectRandom(users);
        const possibleEvaluations = evaluations.filter(evaluation => evaluation.owner.equals(user._id));

        let dates = [
          Utils.getRandomDate(0, 20),
          Utils.getRandomDate(0, 20)
        ];
        dates.sort((a, b) => a.format('YYYYMMDD') - b.format('YYYYMMDD'));

        const survey = new Survey({
          name: `Survey ${i}`,
          description: casual.sentence,
          owner: user._id,
          evaluation: possibleEvaluations.length ? Utils.selectRandom(possibleEvaluations)._id : null,
          openingDate: dates.shift().toDate(),
          closingDate: dates.shift().toDate(),
          public: Utils.getRandomBoolean(survey_public_chance),
          archived: Utils.getRandomBoolean(survey_archived_chance),
        });

        try {
          await survey.save();
          surveys.push(survey);
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
    it(`Adding questions`, async function () {
      for (const survey of surveys) {
        for (let i = 0; i < Utils.getRandomInteger(survey_questions_qty_min, survey_questions_qty_max); i++) {
          survey.questions.push({
            name: `Question ${i}`,
            description: casual.short_description,
            position: i
          });
        }
        try {
          await survey.save();
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
    it(`Adding criteria`, async function () {
      for (const survey of surveys) {
        for (const question of survey.questions) {
          for (let i = 0; i < Utils.getRandomInteger(criteria_qty_min, criteria_qty_max); i++) {
            let criteria;
            const t = Utils.getRandomInteger(0, 100);
            if (t < 25) {
              criteria = {
                type: 'scale',
                minValue: Utils.getRandomInteger(0, 5),
                maxValue: Utils.getRandomInteger(5, 25)
              };
            }
            else if (t < 50) {
              let options = [];
              for (let ii = 0; ii < Utils.getRandomInteger(3, 8); ii++) options.push(`${ii}. ${casual.string}`);
              criteria = {
                type: 'select',
                options: options
              };
            } else if (t < 75) {
              criteria = {
                type: 'text'
              }
            } else if (t <= 100) {
              let options = [];
              for (let ii = 0; ii < Utils.getRandomInteger(3, 8); ii++) options.push(`${ii}. ${casual.string}`);
              criteria = {
                type: 'checkbox',
                options: options
              }
            }
            criteria.name = Utils.selectRandom(criteria_titles);
            question.criteria.push(criteria);
          }
        }
        try {
          await survey.save();
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
    it(`Adding links`, async function () {
      for (const survey of surveys) {
        try {
          await survey.generateLinks();
          await survey.save();
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
  });

  describe(`Answers and sessions`, async function () {
    it(`Adding sessions`, async function () {
      for (const survey of surveys) {
        users = await User.find();
        if (!survey.evaluation) continue;
        const evaluation = await Evaluation.findOne({ _id: survey.evaluation }).populate('groups');
        if (evaluation.groups.length < 1) continue;

        for (let i = 0; i < Utils.getRandomInteger(sessions_qty_min, sessions_qty_max); i++) {
          const group = Utils.selectRandom(evaluation.groups);
          survey.sessions.push({
            group: group._id,
            group_name: group.name,
            open: Utils.getRandomBoolean(0.9),
            owner: (users.length) > 0 ? Utils.selectRandom(users)._id : null,
          });
        }

        try {
          await survey.save();
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
    it(`Adding answers`, async function () {
      for (const survey of surveys) {
        for (const question of survey.questions) {
          for (const session of survey.sessions) {
            const subanswers = [];
            for (const criteria of question.criteria) {
              if (!Utils.getRandomBoolean(sessions_chance_to_answer)) continue;

              const subanswer = {
                criteria: criteria._id,
                name: criteria.name,
                type: criteria.type,
              };
              if (criteria.type === 'scale') subanswer.value = Utils.getRandomInteger(criteria.minValue, criteria.maxValue);
              else if (criteria.type === 'select') subanswer.value = Utils.selectRandom(criteria.options);
              else if (criteria.type === 'checkbox') { //TODO could be completely randomized
                const selected = [];
                for (let i = 0; i < Utils.getRandomInteger(0, criteria.options.length); i++) selected.push(criteria.options[i]);
                subanswer.value = selected;
              }
              else if (criteria.type === 'text') subanswer.value = casual.sentence;

              subanswers.push(subanswer);
            }
            session.answers.push({
              question: question._id,
              name: question.name,
              subanswers: subanswers
            });
          }
        }

        try {
          await survey.save();
        } catch (err) {
          if (logErrors) console.error(err);
        }
      }
    });
  });

});