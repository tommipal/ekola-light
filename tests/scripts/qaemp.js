const models = require('models');
const { Utils, config } = require('common');

const QAEMP = require('./QAEMP.json');

let admin;
const email = config.admin_email;
const pwd = config.admin_password;
const survey_name = 'QAEMP';

before(async function () {
  this.timeout(30000);
  if (!email) throw new Error('You need to set environment variables for admin user for the script to work!');

  await models.load();

  console.info('This script will import a survey with full QAEMP question set. Beginning import in 5 secods..');
  await Utils.delay(5000);
});

describe('QAEMP', async function () {
  it(`Creating 'admin' user if one doesn't exist..`, async function () {
    admin = await User.findOne({ email: email });
    if (!admin) {
      if (!pwd) throw new Error('You need to set environment variables for admin user for the script to work!');
      const password = await User.hashpwd(pwd);
      admin = new User({
        email: email,
        password: password.encrypted,
        iv: password.iv
      });

    }
    admin.verified = true;
    admin.admin = true;
    await admin.save();
  });

  it('Importing questions..', async function () {
    let survey = await Survey.findOne({ owner: admin._id, name: survey_name });

    if (!survey) {
      survey = new Survey({
        name: survey_name,
        description: '',
        owner: admin._id
      });
    } else survey.questions = [];
    survey.public = true;

    let position = 0;
    for (const question of QAEMP) {
      survey.questions.push({
        name: question.name,
        description: question.description,
        position: position
      });
      position++;
      const copy = Utils.last(survey.questions);

      for (const criteria of question.criteria) {
        copy.criteria.push({
          name: criteria.name,
          description: criteria.description,
          minValue: criteria.options.min,
          maxValue: criteria.options.max
        });
      }
    }

    await survey.save();
  });
});