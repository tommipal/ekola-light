const { expect, assert } = require('chai');

const DocumentFactory = require('../documentFactory').DocumentFactory;
const { Utils, config } = require('common');
const models = require('models');

let target;

before(async function () {
  this.timeout(30000);
  if (config.env !== 'development') throw new Error('Tests should only be executed in development environment!');

  await models.load();
});

describe('Question', async function () {
  beforeEach(async function () {
    const survey = await DocumentFactory.getSurvey();
    target = Utils.selectRandom(survey.questions);
  });

  describe(`addCriteria`, async function () {
    it(`Should add new criteria of the selected type`, async function () {
      const types = Criteria.types();
      assert.equal(types.length, 4);

      let expectedCount = target.criteria.length;
      for (const type of types) {
        expectedCount++;
        target.addCriteria(type);
        assert.equal(target.criteria.length, expectedCount);

        const criteria = Utils.last(target.criteria);
        assert.equal(criteria.type, type);
      }
    });

    it(`Should throw if provided with invalid type`, async function() {
      let error;
      try {
        target.addCriteria();
      } catch(err) {
        error = err;
      }

      assert.isDefined(error);
      assert.isTrue(error.message.includes('Found an unrecognized criteria type:'));
    });
  });

});