const { expect, assert } = require('chai');

const DocumentFactory = require('../documentFactory').DocumentFactory;
const { config } = require('common');
const models = require('models');

let target;

before(async function () {
  this.timeout(30000);
  if (config.env !== 'development') throw new Error('Tests should only be executed in development environment!');

  await models.load();
});

describe('User', async function () {
  beforeEach(async function() {
    target = await DocumentFactory.getUser();
  });

  describe(`hashpwd`, async function () {
    it(`Should encrypt the provided string`, async function () {
      const password = 'test';
      const result = await User.hashpwd(password);
      assert.isObject(result, 'Method should return an object containing encrypted password and initialization vector');
      const encrypted = result.encrypted;
      const iv = result.iv;

      // The result should contain both encrypted password..
      assert.isDefined(encrypted, 'Result should contain the encrypted password');
      assert.isString(encrypted, 'Ecrypted password should be a string')
      // ..and initialization vector
      assert.isDefined(iv, 'Result should contain the initialization vector');
      assert.isString(iv, 'Initialization vector should be a string');

      assert.isFalse(encrypted.includes(password), 'Encrypted password should not contain the original password');
      assert.isFalse(iv.includes(password), 'Initialization vector should not containt the original password');
    });
  });

  describe(`match`, async function () {
    it(`Should match given password against an existing user`, async function () {
      const validPwd = 'test';
      const invalidPwd = 'invalid';

      //Invalid pwd
      let match = await User.match(target.email, invalidPwd);
      assert.isFalse(match, 'Incorrect credentials should return false');

      //Valid pwd
      match = await User.match(target.email, validPwd);
      assert.isTrue(match, 'Correct credentials should return true');

      //No pwd
      match = await User.match(target.email);
      assert.isFalse(match, 'Failing to provide a password should return false');
    });
  });

  describe(`isVerified`, async function () {
    it(`Should check the verified status of a user`, async function () {
      const user = await DocumentFactory.getUser();
      assert.isDefined(user.verified);

      let expected = user.verified;
      let actual = await User.isVerified({ email: user.email });
      assert.equal(actual, expected, 'Return value should match the verified status of the user');

      expected = !expected;
      user.verified = expected;
      await user.save();
      actual = await User.isVerified({ email: user.email });
      assert.equal(actual, expected, 'Return value should match the verified status of the user');
    });

    it(`Should return false if no email or id is provided`, async function () {
      const result = await User.isVerified();
      assert.isFalse(result, 'Calling the method without email or id should return false');
    });

    it(`Should return false if invalid email or id is provided`, async function () {
      const invalidId = target._id;
      await User.deleteOne({ _id: invalidId});

      let result = await User.isVerified({ _id: invalidId });
      assert.isFalse(result, 'Calling the method with invalid id should return false');

      result = await User.isVerified({ email: 'invalid email' });
      assert.isFalse(result, 'Calling the method with invalid email should return false');
    });
  });

});