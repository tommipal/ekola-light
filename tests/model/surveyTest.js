const { expect, assert } = require('chai');

const DocumentFactory = require('../documentFactory').DocumentFactory;
const { Utils, config } = require('common');
const models = require('models');

let target;
let ctx;

before(async function () {
  this.timeout(30000);
  if (config.env !== 'development') throw new Error('Tests should only be executed in development environment!');

  await models.load();
});

describe('Survey', async function () {
  beforeEach(async function () {
    target = await DocumentFactory.getSurvey({ addSessions: false });
    ctx = {
      session: {},
      params: {},
      currentUser: function () { return null; },
      t: function (str) { return str; },
      getRoute: function (str) { return str; }
    };
  });

  describe(`copy`, async function () {
    it(`Should throw if not provided with the required parameters`, async function () {
      const id = target._id;
      const owner = target.owner;
      await Survey.deleteOne({ _id: id });

      let error;
      try {
        await Survey.copy(id, owner);
      } catch (err) {
        error = err;
      }
      assert.isDefined(error);
      assert.isTrue(error.message.includes("Survey doesn't seem to exist anymore"));
    });

    it(`Should throw if the survey no longer exists`, async function () {
      let error;
      try {
        await Survey.copy(target);
      } catch (err) {
        error = err;
      }
      assert.isDefined(error);
      assert.isTrue(error.message.includes('Missing parameters'));
    });

    it(`Should copy an existing survey`, async function () {
      assert.isAbove(target.questions.length, 0, 'Survey should contain some questions');

      const copy = await Survey.copy(target, target.owner);

      assert.isDefined(copy, 'Method should return the copied survey');
      assert.equal(copy.description, target.description, 'Method should copy the details of the original survey');
      assert.equal(copy.questions.length, target.questions.length, 'Method should copy all the questions of the original survey');

      for (const question of target.questions) {
        const match = copy.questions.find(match => match.name === question.name);
        assert.isDefined(match, 'There should be a matching question for every copy');
        assert.isFalse(question._id.equals(match._id), 'Each new question should have a fresh ObjectId');
        assert.equal(match.criteria.length, question.criteria.length, 'Method should copy all the criteria of the original question');

        for (const criteria of question.criteria) {
          const c = match.criteria.find(c => c.name === criteria.name);
          assert.isDefined(c, 'There should be a matching criteria for every copy');
          assert.isFalse(criteria._id.equals(c._id), 'Each new criteria should have a fresh ObjectId');

          const msg = 'Details of the copy should match the original criteria';
          assert.equal(c.type, criteria.type, msg);
          assert.equal(c.minValue, criteria.minValue, msg);
          assert.equal(c.maxValue, criteria.maxValue, msg);
          assert.equal(c.options.length, criteria.options.length, msg);
          for (const option of criteria.options) assert.isTrue(c.options.some(o => o === option), msg);
        }
      }
    });
  });

  describe(`generateLinks`, async function () {
    it(`Should generate links for groups of the evaluation`, async function () {
      target = await DocumentFactory.getSurvey({ addLinks: false });

      assert.equal(target.links.length, 0, 'There should be no links by default');
      await target.generateLinks();

      assert.isAbove(target.links.length, 0, 'Method should create links');
      const evaluation = await Evaluation.findOne({ _id: target.evaluation });

      assert.equal(target.links.length, evaluation.groups.length, 'The number of links should match the number of goups in the surveys evaluation');
      for (const group of evaluation.groups) assert.isDefined(target.links.find(link => link.group.equals(group)), 'There should be one link for each group of the survey\'s evaluation');
    });

    it(`Should remove links for groups no longer part of the evaluation `, async function () {
      assert.isAbove(target.links.length, 0, 'There should be links by default');
      const unexpected = target.links.length;

      const evaluation = await Evaluation.findOne({ _id: target.evaluation });
      evaluation.groups = evaluation.groups.slice(2);
      await evaluation.save();

      await target.generateLinks();
      assert.notEqual(target.links.length, unexpected, 'Method should delete some links');

      assert.equal(target.links.length, evaluation.groups.length, 'The number of links should match the number of goups in the surveys evaluation');
      for (const group of evaluation.groups) assert.isDefined(target.links.find(link => link.group.equals(group)), 'There should be one link for each group of the survey\'s evaluation');
    });
  });

  describe(`instantiateSession`, async function () {
    it(`Should create new session if there is no existing one`, async function () {
      assert.isAbove(target.links.length, 0, 'There should be links by default');
      const link = target.links[0];
      ctx.params.link = link._id;

      assert.equal(target.sessions.length, 0, 'There should be no sessions by default');
      const session = await target.instantiateSession(ctx);
      assert.equal(target.sessions.length, 1, 'Method should have created new session');
      assert.isDefined(session, 'Method should have returned the new session');
      assert.isTrue(session.group.equals(link.group), 'New session should use the same group as the link');
      assert.isNull(session.owner, 'Session should be userless, because the ctx did not contain a reference to a user');
    });

    it(`Should use session from the ctx (cookies) if one is available`, async function () {
      assert.isAbove(target.links.length, 0, 'There should be links by default');
      const link = target.links[0];
      ctx.params.link = link._id;

      assert.equal(target.sessions.length, 0, 'There should be no sessions by default');
      let session = await target.instantiateSession(ctx);
      assert.equal(ctx.session.sessions.length, 1, 'Method should have created new session');

      session = await target.instantiateSession(ctx);
      const msg = 'Method should not create an additional session, because there was already an existing session for the same survey in the ctx.session';
      assert.equal(ctx.session.sessions.length, 1, msg);
      assert.equal(target.sessions.length, 1, msg);
      assert.isDefined(session, 'Method should return the session');
      assert.isTrue(session.group.equals(link.group), 'The session should use the same group as the link');
      assert.isNull(session.owner, 'Session should be userless, because the ctx did not contain a reference to a user');
    });

    it(`Should use existing session if current user has one`, async function () {
      const user = await DocumentFactory.getUser();
      ctx.currentUser = function () { return user; }

      assert.isAbove(target.links.length, 0, 'There should be links by default');
      const link = target.links[0];
      ctx.params.link = link._id;

      assert.equal(target.sessions.length, 0, 'There should be sessions by default');
      let session = await target.instantiateSession(ctx);
      assert.equal(target.sessions.length, 1, 'Method should create new session');
      assert.isDefined(session, 'Method should return the new session');
      assert.isTrue(user._id.equals(session.owner), 'Session should have a user, because the ctx contained a reference to one');
      ctx.session.sessions = []; //Remove the session from the ctx

      session = await target.instantiateSession(ctx);
      assert.equal(target.sessions.length, 1, 'Method should not create new session, because there already is a session belonging to the current user');
      assert.isDefined(session, 'Method should return the session');
      assert.isTrue(session.group.equals(link.group), 'The session should use the same group as the link');
    });
  });

  describe(`updateQuestion`, async function () {
    it(`Should update a question of the subdocument array questions`, async function () {
      const question = Utils.selectRandom(target.questions);
      const expected = 'updated';
      question.name = expected;
      const criteria = Utils.selectRandom(question.criteria);
      criteria.name = expected;

      await target.updateQuestion(question._id, question);
      target = await Survey.findOne({ _id: target._id });

      const updatedQuestion = target.questions.id(question._id);
      assert.isDefined(updatedQuestion, 'The question should still be there');
      assert.equal(updatedQuestion.name, expected, 'Method should save the changes made to the question');

      const updatedCriteria = updatedQuestion.criteria.id(criteria._id);
      assert.isDefined(updatedCriteria, 'The criteria should still be there');
      assert.equal(updatedCriteria.name, expected, 'Method should save the changes made to the criteria');
    });
  });

  describe(`getExcel`, async function () {
    it(`Should not throw`, async function () {
      let error;
      try {
        await target.getExcel(ctx);
      } catch (err) {
        error = err;
      }
      assert.isUndefined(error, 'Method should execute without any errors');
    });
  });

  describe(`notify`, async function () {
    it(`Should not throw`, async function () {
      let error;
      try {
        await target.notify(ctx);
      } catch (err) {
        error = err;
      }
      assert.isUndefined(error, 'Method should execute without any errors');
    });
  });

  describe(`getResultsByType`, async function () {
    beforeEach(async function () {
      target = await DocumentFactory.getSurvey({ session_qty: 50 });
    });

    it(`Results of the aggregation should always contain information about the question and criteria it matches to`, async function () {
      const result = await target.getResultsByType('scale');

      assert.isAbove(result.length, 0);
      const document = Utils.selectRandom(result);

      assert.isDefined(document._id.question, 'Aggregation _id should contain the ObjectId of the question');
      assert.isDefined(document._id.question_name, 'Aggregation _id should contain the name of the question');

      assert.isDefined(document._id.criteria, 'Aggregation _id should contain the ObjectId of the criteria');
      assert.isDefined(document._id.criteria_name, 'Aggregation _id should contain the name of the criteria');

      assert.isDefined(document._id.group, 'Aggregation _id should contain the ObjectId of the group');
      assert.isDefined(document._id.group_name, 'Aggregation _id should contain the name of the group');

      assert.isDefined(document.answers, 'Aggregation _id should contain the ObjectIds of the answers');
    });

    it(`Should return aggregated results (Scale)`, async function () {
      const result = await target.getResultsByType('scale');

      assert.isAbove(result.length, 0);
      const evaluation = await Evaluation.findOne({ _id: target.evaluation }).populate('groups');

      for (const group of evaluation.groups) {
        for (const question of target.questions) {
          const sessions = target.sessions.filter(session => session.group.equals(group._id));
          const answers = [];
          for (const session of sessions) answers.push(session.answers.find(answer => answer.question.equals(question._id)));

          for (const criteria of question.criteria.filter(criteria => criteria.type === 'scale')) {
            let value = 0;
            let qty = 0;
            for (const answer of answers) {
              const subanswer = answer.subanswers.find(subanswer => subanswer.criteria.equals(criteria._id));
              value += subanswer.value;
              qty++;
            }
            let avg = value / qty;
            const aggregatedResult = result.find(doc => group._id.equals(doc._id.group) && question._id.equals(doc._id.question) && criteria._id.equals(doc._id.criteria));

            assert.equal(Math.round(avg), Math.round(aggregatedResult.average), 'Average value calculated here should match the value calculated by the aggregation');
          }
        }
      }
    });

    it(`Should return aggregated results (Select)`, async function () {
      const result = await target.getResultsByType('select');
      assert.isAbove(result.length, 0, 'The result should contain multiple documents');

      const evaluation = await Evaluation.findOne({ _id: target.evaluation }).populate('groups');

      for (const group of evaluation.groups) {
        for (const question of target.questions) {
          const sessions = target.sessions.filter(session => session.group.equals(group._id));
          const answers = [];
          for (const session of sessions) answers.push(session.answers.find(answer => answer.question.equals(question._id)));

          for (const criteria of question.criteria.filter(criteria => criteria.type === 'select')) {
            for (const option of criteria.options) {
              let qty = 0;
              for (const answer of answers) {
                const subanswer = answer.subanswers.find(subanswer => subanswer.criteria.equals(criteria._id) && subanswer.value === option);
                if (!subanswer) continue;
                qty++;
              }
              if (qty === 0) continue;

              const aggregatedResult = result.find(doc => group._id.equals(doc._id.group) && question._id.equals(doc._id.question) && criteria._id.equals(doc._id.criteria) && doc._id.value === option);

              assert.equal(qty, aggregatedResult.count, 'The value calculated here should match the value calculated by the aggregation. (The number of times a particular option was selected)');
            }
          }
        }
      }
    });

    it(`Should return aggregated results (Checkbox)`, async function () {
      const result = await target.getResultsByType('checkbox');
      assert.isAbove(result.length, 0, 'The result should contain multiple documents');

      const evaluation = await Evaluation.findOne({ _id: target.evaluation }).populate('groups');

      for (const group of evaluation.groups) {
        for (const question of target.questions) {
          const sessions = target.sessions.filter(session => session.group.equals(group._id));
          const answers = [];
          for (const session of sessions) answers.push(session.answers.find(answer => answer.question.equals(question._id)));

          for (const criteria of question.criteria.filter(criteria => criteria.type === 'checkbox')) {
            for (const option of criteria.options) {
              let qty = 0;
              for (const answer of answers) {
                const subanswer = answer.subanswers.find(subanswer => subanswer.criteria.equals(criteria._id) && subanswer.value === option);
                if (!subanswer) continue;
                qty++;
              }
              if (qty === 0) continue;

              const aggregatedResult = result.find(doc => group._id.equals(doc._id.group) && question._id.equals(doc._id.question) && criteria._id.equals(doc._id.criteria) && doc._id.value === option);

              assert.equal(qty, aggregatedResult.count, 'The value calculated here should match the value calculated by the aggregation. (The number of times a particular option was selected)');
            }
          }
        }
      }
    });

    it(`Should return aggregated results (Text)`, async function () {
      const result = await target.getResultsByType('text');
      assert.isAbove(result.length, 0, 'The result should contain multiple documents');

      const evaluation = await Evaluation.findOne({ _id: target.evaluation }).populate('groups');

      for (const group of evaluation.groups) {
        for (const question of target.questions) {
          const sessions = target.sessions.filter(session => session.group.equals(group._id));
          const answers = [];
          for (const session of sessions) answers.push(session.answers.find(answer => answer.question.equals(question._id)));

          for (const criteria of question.criteria.filter(criteria => criteria.type === 'text')) {
            const texts = [];
            for (const answer of answers) {
              const subanswer = answer.subanswers.find(subanswer => subanswer.criteria.equals(criteria._id));
              if (!subanswer) continue;
              texts.push(subanswer.value);
            }

            const aggregatedResult = result.find(doc => group._id.equals(doc._id.group) && question._id.equals(doc._id.question) && criteria._id.equals(doc._id.criteria));
            assert.equal(texts.length, aggregatedResult.values.length, 'The number of text-answers should match with what was returned by the aggregation');
            for (const text of texts) assert.isTrue(aggregatedResult.values.some(result => result === text), 'Aggregation should return all the text answers');
          }
        }
      }
    });

    it(`Should throw if no type is provided`, async function () {
      let error;
      try {
        await target.getResultsByType();
      } catch (err) {
        error = err;
      }
      assert.isDefined(error);
      assert.isTrue(error.message.includes('Expected a type.'));
    });
  });

  describe(`getResults`, async function () {
    beforeEach(async function () {
      target = await DocumentFactory.getSurvey({ session_qty: 50 });
    });

    it(`Should not throw`, async function () {
      let error;
      try {
        await target.getResults();
      } catch (err) {
        error = err;
      }
      assert.isUndefined(error, 'Method should execute without any errors')
    });

    it(`Should return multiple result documents`, async function () {
      const result = await target.getResults();

      assert.isAbove(result.all.length, 0, 'Results should contain multiple documents');
    });
  });

  describe(`moveUp`, async function () {
    it(`Should lift the position of the position of the question`, async function () {

      const question = target.questions[0];
      assert.equal(question.position, 0);

      for (let i = 0; i < target.questions.length; i++) {
        assert.equal(question.position, i);
        target.moveUp(question);
      }

      target.moveUp(question);
      assert.equal(question.position, (target.questions.length - 1), 'Method should not lift the position beyond the length of questions array');
    });
  });

  describe(`moveDown`, async function () {
    it(`Should lower the position of the question`, async function () {
      const question = Utils.last(target.questions);
      assert.equal(question.position, target.questions.length - 1);

      for (let i = target.questions.length - 1; i >= 0; i--) {
        assert.equal(question.position, i);
        target.moveDown(question);
      }

      target.moveDown(question);
      assert.equal(question.position, 0, 'Method should not lower the position beyond 0');
    });
  });

  describe(`sortQuestions`, async function () {
    it(`Should sort questions by position and remove any gaps between position numbers`, async function () {
      const removed = target.questions[1];
      target.questions.id(removed._id).remove();

      target.sortQuestions();
      for (let i = 0; i < target.questions.length; i++) {
        assert.equal(target.questions[i].position, i, 'Method should remove anyu gaps in the position numbers');
      }
    });
  });

  describe(`removeQuestion`, async function () {
    it(`Should remove a question`, async function () {
      const removed = target.questions[1];
      const length = target.questions.length;
      target.removeQuestion(removed._id);
      assert.equal(target.questions.length, (length - 1), 'Method should remove one document from the questions');
      assert.isFalse(target.questions.some(question => question._id.equals(removed._id)), 'Questions should no longer contain the removed question');
    });
  });
});