const { expect, assert } = require('chai');
const models = require('models');

describe(`Models index`, async function () {
  it(`Should load models without errors and make models globally available`, async function() {
    this.timeout(30000);

    let error;
    try {
      await models.load();
    } catch(err) {
      console.error(`Loading models failed with error:\n${err}`)
      error = err;
    }
    assert.isUndefined(error);

    assert.isDefined(User);
    assert.isDefined(Survey);
    assert.isDefined(Group);
    assert.isDefined(Evaluation);
  });
});