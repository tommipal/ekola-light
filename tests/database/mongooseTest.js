const { expect, assert } = require('chai');
const mongoose = require('mongoose');
const { config } = require('common');

const uri = `mongodb://${config.databaseUser}:${config.databasePassword}@${config.databaseHost}:27017/${config.database}`;

describe(`Mongoose`, async function () {
  it(`Should connect without errors`, async function () {
    this.timeout(30000);

    let error;
    try {
      let promise = mongoose.connect(uri, config.databaseConfig);
      await promise;
    } catch (err) {
      console.error(`Mongoose connection failed with error:\n${err}`);
      error = err;
    }

    assert.isUndefined(error);
  });
});