const textgen = require('casual');

const { Utils } = require('common');

class DocumentFactory {
  constructor() { }

  static getAnswer(question) {
    const answer = {
      question: question._id,
      name: question.name,
      subanswers: []
    }
    for (const criteria of question.criteria) {
      answer.subanswers.push(this.getSubanswer(criteria))
    }

    return answer;
  }
  static getSubanswer(criteria) {
    const subanswer = {
      name: criteria.name,
      type: criteria.type,
      criteria: criteria._id,
    };

    if (criteria.type === 'text') {
      subanswer.value = textgen.sentence;
    } else if (criteria.type === 'scale') {
      subanswer.value = Utils.getRandomInteger(criteria.minValue, criteria.maxValue);
    } else if (criteria.type === 'select') {
      subanswer.value = Utils.selectRandom(criteria.options);
    } else if (criteria.type === 'checkbox') {
      subanswer.value = [];
      for (let i = 0; i < Utils.getRandomInteger(0, criteria.options.length); i++) subanswer.value.push(criteria.options[i]);
    } else throw new Error();

    return subanswer;
  }

  static getCriteria({ name, type } = {}) {
    let criteria;
    if (!type) type = Utils.selectRandom(['scale', 'select', 'checkbox', 'text']);

    if (type === 'scale') {
      criteria = {
        type: 'scale',
        minValue: Utils.getRandomInteger(0, 5),
        maxValue: Utils.getRandomInteger(5, 25)
      };
    } else if (type === 'select') {
      let options = [];
      for (let i = 0; i < Utils.getRandomInteger(3, 5); i++) options.push(`${i}. ${textgen.string}`);
      criteria = {
        type: 'select',
        options: options
      };
    } else if (type === 'text') {
      criteria = {
        type: 'text'
      }
    } else { // Checkbox
      let options = [];
      for (let i = 0; i < Utils.getRandomInteger(3, 5); i++) options.push(`${i}. ${textgen.string}`);
      criteria = {
        type: 'checkbox',
        options: options
      }
    }
    criteria.name = name || textgen.word;

    return criteria;
  }

  static async getEvaluation({ owner, group_qty = 3 } = {}) {
    if (!owner) owner = await this.getUser();
    const evaluation = new Evaluation({
      owner: owner._id || owner,
      name: textgen.word,
      description: textgen.short_description
    });
    for (let i = 0; i < group_qty; i++) {
      const group = await this.getGroup({ owner: owner });
      evaluation.groups.push(group);
    }
    await evaluation.save();

    return evaluation;
  }

  static async getGroup({ owner } = {}) {
    if (!owner) owner = await this.getUser();
    const group = new Group({
      owner: owner._id || owner,
      name: textgen.word,
      description: textgen.short_description
    });
    for (let i = 0; i < Utils.getRandomInteger(1, 10); i++) {
      group.members.push({ name: textgen.name, email: textgen.email });
    }
    await group.save();

    return group;
  }

  static getQuestion({ name, description, position } = {}) {
    const question = {
      name: name || textgen.sentence,
      description: description || textgen.short_description,
      position,
    }

    return question;
  }

  static async getSession({ group, owner = null, open = true } = {}) {
    if (!group) throw new Error();
    const session = {
      group: group._id,
      group_name: group.name,
      owner,
      open,
    }

    return session;
  }

  static async getSurvey(
    { owner, addLinks = true, addSessions = true, addAnswers = true,
      session_qty = 10 } = {}) {
    if (!owner) owner = await this.getUser();

    const evaluation = await this.getEvaluation({ owner });
    const survey = new Survey({
      name: textgen.words(3),
      description: textgen.short_description,
      owner: owner._id || owner,
      evaluation: evaluation._id,
    });
    const types = ['scale', 'select', 'checkbox', 'text'];
    for (let i = 0; i < 3; i++) {
      survey.questions.push(this.getQuestion({ name: `Question ${i}`, position: i }));
      for (let ii = 0; ii < 4; ii++) Utils.last(survey.questions).criteria.push(this.getCriteria({ name: `Criteria ${ii}`, type: types[ii] }));
    }

    if (addLinks) {
      for (const group of evaluation.groups) survey.links.push({
        name: group.name,
        group: group._id,
        identifier: Utils.getRandomString(25),
      });
    }

    if (addSessions) {
      for (let i = 0; i < session_qty; i++) {
        const session = await this.getSession({ group: Utils.selectRandom(evaluation.groups) });
        survey.sessions.push(session);
      }
      if (addAnswers) {
        for (const question of survey.questions) {
          for (const session of survey.sessions) session.answers.push(this.getAnswer(question));
        }
      }
    }

    await survey.save();

    return survey;
  }

  static async getUser({ admin = true, verified = true } = {}) {
    const password = await User.hashpwd('test')
    const user = new User({
      email: textgen.email,
      name: textgen.name,
      verified,
      admin,
      password: password.encrypted,
      iv: password.iv,
    });

    let i = 0;
    const maxTries = 1000;
    while (true && i < maxTries) {
      const dublicate = await User.findOne({ email: user.email });
      if (!dublicate) break;
      user.email += textgen.word;
      i++;
    }
    await user.save();

    return user;
  }

}

exports.DocumentFactory = DocumentFactory;