const { expect, assert } = require('chai');
const { config } = require('common');
const { Translator } = require('extensions');

const target = Translator.Translator;

before(async function () {
});

describe('Translator', async function () {
  beforeEach(async function () {
  });

  describe('translate()', async function () {
    it('Should return translation matching to the requested locale', function () {
      let actual = target.translate('user.profile.title', 'en');
      assert.equal(actual, 'Profile');

      actual = target.translate('user.profile.title', 'fi');
      assert.equal(actual, 'Profiili');
    });

    it('Should replace variables inside of a translation if provided with values', function () {
      const link = 'valueToBePlaced';
      let expected = `Copied link: ${link}`;
      const actual = target.translate('survey.edit.linkCopied', 'en', { link });

      assert.equal(actual, expected, 'Method should replace the :link variable with the given value');
    });
  });
});