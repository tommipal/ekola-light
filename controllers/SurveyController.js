const fs = require('fs');
const moment = require('moment');
const Router = require('koa-router');
const send = require('koa-send');
const Controller = require('./Controller');
const { SurveyForm, AnswerForm } = require('forms');
const { Authenticator } = require('middleware');

const router = new Router();

class SurveyController extends Controller {
  /**
   * TODO can this be simplified further?
   */
  static async answer(ctx, next) {
    const survey = await Survey.findById(ctx.params.id).populate('evaluation');

    const session = await survey.instantiateSession(ctx);
    const form = await AnswerForm.build(ctx, survey, session);

    if (await form.isValid()) {
      await form.getData(survey, session);
    }

    return ctx.view('survey/answer', { form, survey, link: survey.links.id(ctx.params.link) });
  }

  /**
   *
   */
  static async archive(ctx, next) {
    const status = (ctx.params.status === 'true') ? true : false;

    await Survey.updateOne({ _id: ctx.params.id }, { archived: status });

    return ctx.redirect(status ? ctx.getRoute('user.profile') : ctx.getRoute('survey.edit', { id: ctx.params.id }));
  }

  static async copy(ctx, next) {
    const survey = await Survey.copy(ctx.params.id, ctx.currentUser()._id);
    await survey.save();

    return ctx.redirect(ctx.getRoute('survey.edit', { id: survey._id }));
  }

  /**
   *
   */
  static async create(ctx, next) {
    const survey = new Survey({ name: "New Survey", owner: ctx.currentUser()._id });
    await survey.save();

    return ctx.redirect(ctx.getRoute('survey.edit', { id: survey._id }));
  }

  /**
   *
   */
  static async delete(ctx, next) {
    await Survey.deleteOne({ _id: ctx.request.body.survey });
    ctx.getFlashMessanger().showMessage('survey.edit.deleted');

    return ctx.redirect(ctx.getRoute('user.profile'));
  }

  /**
   *
   */
  static async edit(ctx, next) {
    let survey = await Survey.findById(ctx.params.id);
    const evaluation = await Evaluation.findOne({ _id: survey.evaluation }).populate('groups');
    const form = await SurveyForm.build(ctx, survey);

    if (await form.isValid()) {
      survey = await form.getData();
      await survey.save();
      ctx.getFlashMessanger().showMessage('survey.edit.saved');
    }
    await survey.generateLinks();

    return ctx.view('survey/edit', { form, survey, evaluation });
  }

  /**
   *
   */
  static async excel(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    const file = await survey.getExcel(ctx, `${survey.name}.xlsx`);

    ctx.attachment(`${survey.name} (${ctx.t('survey.results.title')}) ${moment().format('DD-MM-YYYY')}.xlsx`);

    await send(ctx, `./${file}`);

    fs.unlink(`./${file}`, (err) => { if(err) console.error(err); });
  }

  /**
   *
   */
  static async notify(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    await survey.notify(ctx);
    ctx.getFlashMessanger().showMessage('survey.edit.notificationSent');

    return ctx.redirect(ctx.getRoute('survey.edit', { id: ctx.params.id }, 'sharing_panel'));
  }

  /**
   *
   */
  static async results(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    const evaluation = await Evaluation.findOne({ _id: survey.evaluation }).populate('groups');
    const groups = evaluation ? evaluation.groups : [];

    return ctx.view('survey/results', { survey, evaluation, groups });
  }

  /**
   *
   */
  static async results_data(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    const evaluation = await Evaluation.findOne({ _id: survey.evaluation }).populate('groups');
    const results = await survey.getResults();

    ctx.body = { results: results.all, survey, groups: evaluation ? evaluation.groups : [] };

    return next();
  }

  /**
   *
   */
  static async refreshLinks(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    await survey.generateLinks();
    await survey.save();

    return ctx.redirect(ctx.getRoute('survey.edit', { id: survey._id }, 'sharing_panel'));
  }

  static async view(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    const evaluation = await Evaluation.findOne({ _id: survey.evaluation }).populate('groups');

    return ctx.view('survey/view', { survey, evaluation });
  }

  static async view_data(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    const evaluation = await Evaluation.findOne({ _id: survey.evaluation }).populate('groups');

    ctx.body = { survey, evaluation };

    return next();
  }

  /**
   *
   */
  static async viewArchived(ctx, next) {
    const surveys = await Survey.find({ owner: ctx.currentUser()._id, archived: true });

    return ctx.view('survey/archived', { surveys });
  }

  /**
   *
   */
  static async viewPublic(ctx, next) {
    const surveys = await Survey.find({ public: true });

    return ctx.view('survey/public', { surveys });
  }

  /**
   *
   */
  static async _exists(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    if (!survey) {
      ctx.getFlashMessanger().showAlert('Could not find requested survey..', 'Error');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }

    return await next();
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/survey');

    router.all('survey.answer',        '/:id/answer/:link/:identifier', Authenticator.verifyLink, this.answer);

    router.use(Authenticator.login);
    router.get('survey.archive',       '/:id/archive/:status', this._exists,                this.archive);
    router.get('survey.archived.view', '/archived',                                         this.viewArchived);
    router.get('survey.copy',          '/:id/copy',            Authenticator.survey_public, this.copy);
    router.get('survey.create',        '/create',                                           this.create);
    router.post('survey.delete',       '/delete',              Authenticator.survey,        this.delete);
    router.all('survey.edit',          '/:id/edit',            Authenticator.survey,        this.edit);
    router.get('survey.excel',         '/:id/excel',           Authenticator.survey,        this.excel);
    router.get('survey.links.refresh', '/:id/links/refresh',   Authenticator.survey,        this.refreshLinks);
    router.get('survey.notify',        '/:id/notify',          Authenticator.survey,        this.notify);
    router.get('survey.view',          '/:id/view',            Authenticator.survey_public, this.view);
    router.get('survey.view.data',     '/:id/view/data',       Authenticator.survey_public, this.view_data)
    router.get('survey.public.view',   '/public',                                           this.viewPublic);
    router.get('survey.results',       '/:id/results',         Authenticator.survey_public, this.results);
    router.get('survey.results.data',  '/:id/results/data',    this._exists,                this.results_data);

    return router;
  }
}

exports.controller = SurveyController;