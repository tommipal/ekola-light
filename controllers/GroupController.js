const Router = require('koa-router');
const { GroupForm, GroupMemberForm } = require('forms');
const Controller = require('./Controller');
const { Authenticator } = require('middleware');

const router = new Router();

class GroupController extends Controller {
  /**
   *
   */
  static async archive(ctx, next) {
    const status = (ctx.params.status === 'true') ? true : false;

    await Group.updateOne({ _id: ctx.params.id }, { archived: status });

    return ctx.redirect(status ? ctx.getRoute('user.profile') : ctx.getRoute('group.edit', { id: ctx.params.id }));
  }

  /**
   *
   */
  static async create(ctx, next) {
    const group = new Group({ owner: ctx.currentUser()._id, name: 'New Group' });
    await group.save();

    return ctx.redirect(ctx.getRoute('group.edit', { id: group.id }));
  }

  /**
   *
   */
  static async createMember(ctx, next) {
    const group = await Group.findOne({ _id: ctx.params.id });
    const form = await GroupMemberForm.build(ctx);

    if (await form.isValid()) {
      const member = await form.getData();
      group.members.push(member);
      await group.save();

      return ctx.redirect(ctx.getRoute('group.edit', { id: group._id }));
    }

    return ctx.view('group/member/create', { form, group })
  }

  /**
   * Removes the reference of the group from all the user's evaluations and deletes the group
   */
  static async delete(ctx, next) {
    const id = ctx.request.body.group;
    const evaluations = await Evaluation.find({ owner: ctx.currentUser()._id });
    for (const evaluation of evaluations) {
      evaluation.groups = evaluation.groups.filter(group => !group.equals(id));
      await evaluation.save();
    }
    await Group.deleteOne({ _id: id });
    ctx.getFlashMessanger().showMessage('group.edit.deleted');

    return ctx.redirect(ctx.getRoute('user.profile'));
  }

  /**
   *
   */
  static async edit(ctx, next) {
    let group =  await Group.findOne({ _id: ctx.params.id });
    const form = await GroupForm.build(ctx, group);

    if (await form.isValid()) {
      group = await form.getData();
      await group.save();
      ctx.getFlashMessanger().showMessage('group.edit.saved');
    }

    return ctx.view('group/edit', { form, group });
  }

  /**
   *
   */
  static async editMember(ctx, next) {
    const group = await Group.findOne({ _id: ctx.params.id });
    let member = group.members.id(ctx.params.member);
    const form = await GroupMemberForm.build(ctx, member);

    if (await form.isValid()) {
      member = await form.getData();
      group.members.id(ctx.params.member).remove();
      group.members.push(member);
      await group.save();

      return ctx.redirect(ctx.getRoute('group.edit', { id: group.id }));
    }

    return ctx.view('group/member/edit', { form, group, member })
  }

  /**
   *
   */
  static async removeMember(ctx, next) {
    const group = await Group.findOne({ _id: ctx.params.id });
    group.members.id(ctx.params.member).remove();
    await group.save();

    return ctx.redirect(ctx.getRoute('group.edit', { id: group._id }));
  }

    /**
   *
   */
  static async viewArchived(ctx, next) {
    const groups = await Group.find({ owner: ctx.currentUser()._id, archived: true });

    return ctx.view('group/archived', { groups });
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/group');

    router.use(Authenticator.login);
    router.get('group.archive',        '/:id/archive/:status',        Authenticator.group, this.archive);
    router.get('group.archived.view',  '/archived',                                        this.viewArchived);
    router.get('group.create',         '/create',                                          this.create);

    router.post('group.delete',        '/delete',                     Authenticator.group, this.delete);
    router.all('group.edit',           '/:id/edit',                   Authenticator.group, this.edit);

    router.all('group.members.create', '/:id/members/add',            Authenticator.group, this.createMember);
    router.all('group.members.delete', '/:id/members/:member/delete', Authenticator.group, this.removeMember);
    router.all('group.members.edit',   '/:id/members/:member/edit',   Authenticator.group, this.editMember);

    return router;
  }
}

exports.controller = GroupController;