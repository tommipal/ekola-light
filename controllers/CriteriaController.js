const Router = require('koa-router');
const Controller = require('./Controller');
const { Authenticator } = require('middleware');
const { CriteriaForm } = require('forms');

const router = new Router();

class QuestionController extends Controller {
  /**
   *
   */
  static async create(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    const question = survey.questions.id(ctx.params.question);

    question.addCriteria(ctx.params.type);
    await survey.save();

    return ctx.redirect(ctx.getRoute('survey.questions.criteria.edit', { id: ctx.params.id, question: ctx.params.question, criteria: question.criteria[question.criteria.length - 1]._id }));
  }

  /**
   *
   */
  static async delete(ctx, next) {
    const survey = await Survey.findById(ctx.params.id);
    const question = survey.questions.id(ctx.params.question);

    question.criteria.id(ctx.request.body.criteria).remove();
    await survey.save();
    ctx.getFlashMessanger().showMessage('criteria.edit.deleted');

    return ctx.redirect(ctx.getRoute('survey.questions.edit', { id: ctx.params.id, question: ctx.params.question }));
  }

  /**
   *
   */
  static async edit(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    const question = survey.questions.id(ctx.params.question);
    let criteria = question.criteria.id(ctx.params.criteria);

    const form = await CriteriaForm.build(ctx, criteria, question, survey);

    if (await form.isValid()) {
      await form.getData();
      await survey.save();
      ctx.getFlashMessanger().showMessage('criteria.edit.saved');

      return ctx.redirect(ctx.getRoute('survey.questions.edit', { id: ctx.params.id, question: ctx.params.question }));
    }

    return ctx.view('criteria/edit', { form, survey, question, criteria });
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/survey');
    router.use(Authenticator.login);

    router.get('survey.questions.criteria.create',  '/:id/questions/:question/criteria/create/:type',   Authenticator.survey, this.create);
    router.post('survey.questions.criteria.delete', '/:id/questions/:question/criteria/delete/',        Authenticator.survey, this.delete);
    router.all('survey.questions.criteria.edit',    '/:id/questions/:question/criteria/:criteria/edit', Authenticator.survey, this.edit);

    return router;
  }
}

exports.controller = QuestionController;