const Router = require('koa-router');
const { UserForm, LoginForm } = require('forms');
const Controller = require('./Controller');
const { Authenticator } = require('middleware');

const router = new Router();

class UserController extends Controller {
  /**
   *
   */
  static async admin(ctx, next) {
    const user = await User.findOne({ _id: ctx.request.body.user });
    user.admin = ctx.params.status === 'true' ? true : false;
    await user.save();

    return ctx.redirect(ctx.getRoute('user.list'));
  }

  /**
   *
   */
  static async list(ctx, next) {
    const users = await User.find({ verified: true });

    return ctx.view('user/list', { users });
  }

  /**
   *
   */
  static async listUnverified(ctx, next) {
    const users = await User.find({ verified: false });

    return ctx.view('user/listUnverified', { users });
  }

  /**
   *
   */
  static async login(ctx, next) {
    const form = new LoginForm(ctx);
    if (await form.isValid(ctx)) {
      const user = await form.getData();
      delete user.password;
      delete user.iv;
      ctx.session.user = user;
      console.info(`Successful login for: ${user.email}`);

      return ctx.redirect(ctx.getRoute('user.profile'));
    }

    return ctx.view('user/login', { form });
  }

  /**
   *
   */
  static async logout(ctx, next) {
    ctx.session.user = null;

    return ctx.redirect(ctx.getRoute('user.login'));
  }

  /**
   *
   */
  static async profile(ctx, next) {
    const groups = await Group.find({ owner: ctx.currentUser()._id, archived: false });
    const evaluations = await Evaluation.find({ owner: ctx.currentUser()._id, archived: false });
    const surveys = await Survey.find({ owner: ctx.currentUser()._id, archived: false });

    return ctx.view('user/profile', { groups, evaluations, surveys });
  }

  /**
   *
   */
  static async register(ctx, next) {
    const form = new UserForm(ctx);
    if (await form.isValid()) {
      const user = await form.getData();
      if (user) {
        ctx.getFlashMessanger().showMessage('user.successfullyRegistered');
        return ctx.redirect(ctx.getRoute('user.login'));
      }
    }

    return ctx.view('user/register', { form });
  }

  /**
   *
   */
  static async verify(ctx, next) {
    const id = ctx.request.body.user;
    if (id) {
      const user = await User.findOne({ _id: id, verified: false });
      if (user) {
        user.verified = true;
        await user.save();
      }
    }

    return ctx.redirect(ctx.getRoute('user.list.unverified'));
  }

  static async reject(ctx, next) {
    const id = ctx.request.body.user;
    if (id) {
      await User.deleteOne({ _id: id, verified: false });
    }

    return ctx.redirect(ctx.getRoute('user.list.unverified'));
  }

  /**
   * Middleware for verifying that the current user has admin privileges
   */
  static async isAdmin(ctx, next) {
    const user = await User.findOne({ _id: ctx.currentUser()._id });
    if (!user.admin) {
      ctx.getFlashMessanger().showAlert('error.forbidden');

      return ctx.redirect(ctx.getRoute('user.profile'));
    }

    return await next();
  }

  /**
   * Middleware for verifying that the target user exists
   */
  static async exists(ctx, next) {
    const user = await User.findOne({ _id: ctx.params.id || ctx.request.body.user });
    if (!user) {
      ctx.getFlashMessanger().showAlert('error.userWasNotFound');
      return ctx.redirect(ctx.getRoute('user.profile'));
    }

    return next();
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/user');

    router.all('user.login',           '/login',              Authenticator.isLoggedin, this.login);
    router.all('user.register',        '/register',           Authenticator.isLoggedin, this.register);

    router.use(Authenticator.login);
    router.get('user.list',            '/list',                           this.isAdmin, this.list);
    router.get('user.list.unverified', '/unverified/list',                this.isAdmin, this.listUnverified);
    router.get('user.logout',          '/logout',                                       this.logout);
    router.get('user.profile',         '/profile',                                      this.profile);
    router.post('user.reject',         '/reject',            this.isAdmin, this.exists, this.reject);
    router.post('user.verify',         '/verify',            this.isAdmin, this.exists, this.verify);
    router.post('user.admin',          '/admin/:status',     this.isAdmin, this.exists, this.admin);

    return router;
  }
}

exports.controller = UserController;