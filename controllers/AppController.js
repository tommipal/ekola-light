const Router = require('koa-router');
const Controller = require('./Controller');
const { Authenticator } = require('middleware');

const router = new Router();

class UserController extends Controller {
  static async index(ctx, next) {

    return ctx.redirect(ctx.getRoute('user.profile'));
  }

  static async locale(ctx, next) {
    const locale = ctx.params.locale;
    if (!ctx.supportedLocales().some(supportedLocale => supportedLocale === locale)) {
      ctx.getFlashMessanger().showAlert(`Locale "${locale}" is not supported!`, 'Error');
    } else {
      ctx.session.locale = locale;
    }

    return ctx.redirect('back');
  }

  static get router() {
    if (router.stack.length > 0) return router;

    router.get('app.locale', '/locale/:locale', this.locale);

    router.use(Authenticator.login);
    router.all('app.index',  '/',               this.index);

    return router;
  }
}

exports.controller = UserController;