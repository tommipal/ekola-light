const Router = require('koa-router');
const { EvaluationForm } = require('forms');
const Controller = require('./Controller');
const { Authenticator } = require('middleware');

const router = new Router();

class EvaluationController extends Controller {
  static async archive(ctx, next) {
    const status = (ctx.params.status === 'true') ? true : false;

    await Evaluation.updateOne({ _id: ctx.params.id }, { archived: status });

    return ctx.redirect(status ? ctx.getRoute('user.profile') : ctx.getRoute('evaluation.edit', { id: ctx.params.id }));
  }

  /**
   * TODO change to use a form
   */
  static async addGroup(ctx, next) {
    const evaluation = await Evaluation.findOne({ _id: ctx.params.id });
    const groups = ctx.request.body.groups;
    if (groups) {
      if (groups instanceof Array) for (const group of groups) evaluation.groups.addToSet(group);
      else evaluation.groups.addToSet(groups);
      await evaluation.save();
    }

    return ctx.redirect(ctx.getRoute('evaluation.edit', { id: ctx.params.id }));
  }

  /**
   *
   */
  static async create(ctx, next) {
    const evaluation = new Evaluation({ owner: ctx.currentUser()._id, name: 'New Evaluation' });
    await evaluation.save();

    return ctx.redirect(ctx.getRoute('evaluation.edit', { id: evaluation.id }));
  }

  /**
   *
   */
  static async delete(ctx, next) {
    const evaluation = ctx.request.body.evaluation;
    await Survey.updateMany({ evaluation: evaluation }, { $set: { evaluation: null } });
    await Evaluation.deleteOne({ _id: evaluation });
    ctx.getFlashMessanger().showMessage('evaluation.edit.deleted');

    return ctx.redirect(ctx.getRoute('user.profile'));
  }

  /**
   *
   */
  static async edit(ctx, next) {
    let evaluation = await Evaluation.findOne({ _id: ctx.params.id }).populate('groups');
    const groups = await Group.find({ owner: ctx.currentUser()._id, archived: false, _id: { $nin: evaluation.groups.map(group => group._id) } });
    const form = await EvaluationForm.build(ctx, evaluation);

    if (await form.isValid()) {
      evaluation = await form.getData();
      await evaluation.save();
      ctx.getFlashMessanger().showMessage('evaluation.edit.saved');
    }

    return ctx.view('evaluation/edit', { form, evaluation, groups });
  }

  /**
   *
   */
  static async removeGroup(ctx, next) {
    const evaluation = await Evaluation.findOne({ _id: ctx.params.id });

    evaluation.groups = evaluation.groups.filter(group => group._id.toString() !== ctx.params.group);
    await evaluation.save();

    return ctx.redirect(ctx.getRoute('evaluation.edit', { id: evaluation._id }));
  }

  /**
   *
   */
  static async viewArchived(ctx, next) {
    const evaluations = await Evaluation.find({ owner: ctx.currentUser()._id, archived: true });

    return ctx.view('evaluation/archived', { evaluations });
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/evaluation');
    router.use(Authenticator.login);

    router.get('evaluation.archive',       '/:id/archive/:status',      Authenticator.evaluation, this.archive);
    router.get('evaluation.archived.view', '/archived',                                           this.viewArchived);
    router.get('evaluation.create',        '/create',                                             this.create);
    router.post('evaluation.delete',       '/delete',                   Authenticator.evaluation, this.delete);
    router.all('evaluation.edit',          '/:id/edit',                 Authenticator.evaluation, this.edit);

    router.all('evaluation.groups.add',    '/:id/groups/add',           Authenticator.evaluation, this.addGroup);
    router.all('evaluation.groups.remove', '/:id/groups/:group/remove', Authenticator.evaluation, this.removeGroup);

    return router;
  }
}

exports.controller = EvaluationController;