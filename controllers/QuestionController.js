const Router = require('koa-router');
const Controller = require('./Controller');
const { Authenticator } = require('middleware');
const { QuestionForm } = require('forms');

const router = new Router();

class QuestionController extends Controller {
  /**
   *
   */
  static async create(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    survey.addQuestion();
    await survey.save();

    return ctx.redirect(ctx.getRoute('survey.questions.edit', { id: survey._id, question: survey.questions[survey.questions.length-1]._id }));
  }

  /**
   *
   */
  static async delete(ctx, next) {
    const survey = await Survey.findById(ctx.params.id);
    survey.removeQuestion(ctx.request.body.question);
    await survey.save();
    ctx.getFlashMessanger().showMessage('question.edit.deleted');

    return ctx.redirect(ctx.getRoute('survey.edit', { id: survey._id }, 'questions_panel'));
  }

  /**
   *
   */
  static async edit(ctx, next) {
    const survey = await Survey.findById({ _id: ctx.params.id });
    let question = survey.questions.id(ctx.params.question);

    const form = await QuestionForm.build(ctx, question);
    if (await form.isValid()) {
     question = await form.getData();
     await survey.updateQuestion(question._id, question);
     ctx.getFlashMessanger().showMessage('question.edit.saved');
    }

    return ctx.view('question/edit', { form, question, survey });
  }

  /**
   *
   */
  static async move(ctx, next) {
    const survey = await Survey.findOne({ _id: ctx.params.id });
    if (ctx.params.direction == 'up') survey.moveUp(ctx.params.question);
    else survey.moveDown(ctx.params.question);
    await survey.save();

    return ctx.redirect(ctx.getRoute('survey.edit', { id: ctx.params.id }, 'questions_panel'));
  }

  static get router() {
    if (router.stack.length > 0) return router;
    router.prefix('/survey');
    router.use(Authenticator.login);

    router.all('survey.questions.add',     '/:id/questions/add',                       Authenticator.survey, this.create);
    router.all('survey.questions.edit',    '/:id/questions/:question/edit',            Authenticator.survey, this.edit);
    router.post('survey.questions.delete', '/:id/questions/delete',                    Authenticator.survey, this.delete);
    router.all('survey.questions.move',    '/:id/questions/:question/move/:direction', Authenticator.survey, this.move);

    return router;
  }
}

exports.controller = QuestionController;