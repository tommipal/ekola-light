const koa = require('koa');
const bodyParser = require('koa-bodyparser');
const session = require('koa-session');
const CSRF = require('koa-csrf');
const serve = require('koa-static');
const views = require('koa-views');
const koaWebpack = require('koa-webpack');
const path = require('path');

const { config } = require('common');
const { FlashMessanger, translator_ctx } = require('extensions');
const controllers = require('controllers');
const models = require('models');
const { Responder } = require('middleware');

const app = new koa();
app.keys = config.keys.session;

(async function init() {
  await models.load();

  app.use(session(config.session, app));

  app.use(bodyParser());

  app.use(views(config.appRoot + '/views', { extension: 'ejs' }));

  app.use(serve(path.join(config.appRoot, 'dist')));

  app.use(new CSRF(config.csrf));

  if (config.env === 'development') {
    app.use(await koaWebpack());
  }

  app.use(Responder.handle);

  controllers.load(app);

  // Extend the context
  FlashMessanger.load(app);
  translator_ctx.load(app);

  // Extend the context's render function
  app.context.view = function (view, params = {}) {
    params.ctx = this; // Always pass the context to the view

    return this.render(view, params);
  }

  app.context.currentUser = function () {
    return this.session.user;
  }

  app.on('error', (err, ctx) => {
    console.error('server error', err, ctx);
  });

  app.listen(3000);
  console.info('\nApplication running on port 3000');
})();